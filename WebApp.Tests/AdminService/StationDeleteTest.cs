﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApp.BLL.Infrastructure;

namespace WebApp.Tests.AdminService
{

    public class StationDeleteTest
    {
        private WebApp.BLL.Services.AdminService adminService { get; set; }
        private WebApp.Tests.Database.TestUnitOfWork Database { get; set; } =
            new WebApp.Tests.Database.TestUnitOfWork();


        [ExpectedException(typeof(ValidationException))]
        public void DeleteStationTest()
        {
            var stationIdToDelete = 1;
            adminService.DeleteStation(stationIdToDelete);

            var station = Database.StationsRepository.Get(stationIdToDelete);

            Assert.AreEqual(station.IsDeleted, true);

            adminService.CreateStation(new BLL.DTO.Admin.StationAdminDTO()
            {
                Title = station.Title,
                IsDeleted = false
            });

            Database.StationsRepository.Get(stationIdToDelete).IsDeleted = false;
            Database.Save();
        }
        public StationDeleteTest()
        {
            adminService = new BLL.Services.AdminService(Database);
        }
    }
}
