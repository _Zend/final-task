﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebApp.BLL.DTO;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;
using WebApp.Tests.Database;

namespace WebApp.Tests.TrainService
{
    [TestClass]
    public class FindTrips
    {
        [TestMethod]
        public void GetTrainByTripId()
        {
            var trainService = new BLL.Services.TrainService(new TestUnitOfWork());

            var train = trainService.GetTrainByTripId(1);
            
            Assert.IsNotNull(train);
            Assert.AreEqual(train.Id, 1);
        }
    }
}
