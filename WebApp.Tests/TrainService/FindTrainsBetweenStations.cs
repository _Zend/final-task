﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebApp.BLL.DTO;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;
using WebApp.Tests.Database;

namespace WebApp.Tests.TrainService
{
    [TestClass]
    public class FindTrainsBetweenStations
    {
        [TestMethod]
        public void FindTrainsBetweenStartEndStations()
        {
            Mock<IUnitOfWork> mock = new Mock<IUnitOfWork>();
            mock.Setup(t => t.TrainsRepository.GetAll()).Returns(new List<Train>()
            {
                new Train() { Id = 1, Title = "Demchenko Express", IsDeleted = false },
                new Train() { Id = 2, Title = "Demchenko Slow", IsDeleted = false },
                new Train() { Id = 2, Title = "Demchenko Fast", IsDeleted = false }
            });

            var trainsList = mock.Object.TrainsRepository.GetAll().ToList();

            Assert.AreEqual(trainsList.Count, 3);
        }
        [TestMethod]
        public void FindTrainsBetweenStartAndIntermediateStations()
        {
            var trainService = new BLL.Services.TrainService(new TestUnitOfWork());

            var trains = trainService.Find(new StationsDTO()
            {
                Date = DateTime.Parse("20.05.2017"),
                StationFrom = new StationDTO()
                {
                    Title = "Харьков"
                },
                StationTo = new StationDTO()
                {
                    Title = "Днепр"
                }
            });

            Assert.IsNotNull(trains);

            foreach (var tripDto in trains)
            {
                Assert.AreEqual(tripDto.StationBoarding.Title, "Харьков");
                Assert.AreEqual(tripDto.StationArriving.Title, "Днепр");
            }
        }
        [TestMethod]
        public void FindTrainsBetweenIntermediateAndEndStations()
        {
            var trainService = new BLL.Services.TrainService(new TestUnitOfWork());

            var trains = trainService.Find(new StationsDTO()
            {
                Date = DateTime.Parse("20.05.2017"),
                StationFrom = new StationDTO()
                {
                    Title = "Днепр"
                },
                StationTo = new StationDTO()
                {
                    Title = "Киев"
                }
            });

            Assert.IsNotNull(trains);

            foreach (var tripDto in trains)
            {
                Assert.AreEqual(tripDto.StationBoarding.Title, "Днепр");
                Assert.AreEqual(tripDto.StationArriving.Title, "Киев");
            }
        }
        [TestMethod]
        public void FindTrainsBetweenIntermediateStations()
        {
            var trainService = new BLL.Services.TrainService(new TestUnitOfWork());

            var trains = trainService.Find(new StationsDTO()
            {
                Date = DateTime.Parse("20.05.2017"),
                StationFrom = new StationDTO()
                {
                    Title = "Полтава"
                },
                StationTo = new StationDTO()
                {
                    Title = "Сумы"
                }
            });

            Assert.IsNotNull(trains);

            foreach (var tripDto in trains)
            {
                Assert.AreEqual(tripDto.StationBoarding.Title, "Полтава");
                Assert.AreEqual(tripDto.StationArriving.Title, "Сумы");
            }
        }
    }
}
