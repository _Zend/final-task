﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.DAL.Models.Domain;
using WebApp.DAL.Models.Identity;

namespace WebApp.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        ApplicationRoleManager RoleManager { get; }

        ApplicationUserManager UserManager { get; }

        IRepository<Carriage> CarriagesRepository { get; }

        IRepository<Route> RoutesRepository { get; }

        IRepository<Seat> SeatsRepository { get; }

        IRepository<StationRoute> StationRoutesRepository { get; }

        IRepository<Station> StationsRepository { get; }

        IRepository<Ticket> TicketsRepository { get; }

        IRepository<Train> TrainsRepository { get; }

        IRepository<Trip> TripsRepository { get; }

        IRepository<TicketInfo> TicketInfosRepository { get; }

        void Save();
    }
}
