using WebApp.DAL.EF;
using WebApp.DAL.Models.Identity;

namespace WebApp.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "WebApp.DAL.Models.ApplicationContext";
        }

        protected override void Seed(ApplicationContext context)
        {

        }
    }
}
