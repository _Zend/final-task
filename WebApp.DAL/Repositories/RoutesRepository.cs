﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApp.DAL.EF;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;

namespace WebApp.DAL.Repositories
{
    public class RoutesRepository : IRepository<Route>
    {
        private ApplicationContext _database;

        public RoutesRepository(ApplicationContext database)
        {
            _database = database;
        }

        public IEnumerable<Route> GetAll()
        {
            return _database.Routes.Include(t => t.StationFrom).Include(t => t.StationTo).Where(t => t.IsDeleted == false).ToList();
        }

        public Route Get(int id)
        {
            return _database.Routes.Find(id);
        }

        public IEnumerable<Route> Find(Func<Route, bool> predicate)
        {
            return _database.Routes.Include(t => t.StationFrom).Include(t => t.StationTo).Where(t => t.IsDeleted == false).Where(predicate).ToList();
        }

        public void Create(Route item)
        {
            _database.Routes.Add(item);
        }

        public void Update(Route item)
        {
            _database.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var route = _database.Routes.Find(id);
            if (route != null)
            {
                route.IsDeleted = true;
            }
        }
    }
}