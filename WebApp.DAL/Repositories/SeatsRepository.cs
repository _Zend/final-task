﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApp.DAL.EF;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;

namespace WebApp.DAL.Repositories
{
    public class SeatsRepository : IRepository<Seat>
    {
        private ApplicationContext _database;

        public SeatsRepository(ApplicationContext database)
        {
            _database = database;
        }

        public IEnumerable<Seat> GetAll()
        {
            return _database.Seats.Where(t => t.IsDeleted == false).Include(t => t.Carriage).ToList();
        }

        public Seat Get(int id)
        {
            return _database.Seats.Find(id);
        }

        public IEnumerable<Seat> Find(Func<Seat, bool> predicate)
        {
            return _database.Seats.Where(t => t.IsDeleted == false).Include(t => t.Carriage).Where(predicate).ToList();
        }

        public void Create(Seat item)
        {
            _database.Seats.Add(item);
        }

        public void Update(Seat item)
        {
            _database.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var seat = _database.Seats.Find(id);
            if (seat != null)
            {
                seat.IsDeleted = true;
            }
        }
    }
}