﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApp.DAL.EF;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;

namespace WebApp.DAL.Repositories
{
    public class TripsRepository : IRepository<Trip>
    {
        private ApplicationContext _database;

        public TripsRepository(ApplicationContext database)
        {
            _database = database;
        }

        public IEnumerable<Trip> GetAll()
        {
            return _database.Trips.Where(t => t.IsDeleted == false).Include(t => t.Route).Include(t => t.Train).ToList();
        }

        public Trip Get(int id)
        {
            return _database.Trips.Find(id);
        }

        public IEnumerable<Trip> Find(Func<Trip, bool> predicate)
        {
            return _database.Trips.Where(t => t.IsDeleted == false).Include(t => t.Route).Include(t => t.Train).Where(predicate).ToList();
        }

        public void Create(Trip item)
        {
            _database.Trips.Add(item);
        }

        public void Update(Trip item)
        {
            _database.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var trip = _database.Trips.Find(id);
            if (trip != null)
            {
                trip.IsDeleted = true;
            }
        }
    }
}