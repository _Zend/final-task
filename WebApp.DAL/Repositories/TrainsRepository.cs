﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApp.DAL.EF;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;

namespace WebApp.DAL.Repositories
{
    public class TrainsRepository : IRepository<Train>
    {
        private ApplicationContext _database;

        public TrainsRepository(ApplicationContext database)
        {
            _database = database;
        }

        public IEnumerable<Train> GetAll()
        {
            return _database.Trains.Where(t => t.IsDeleted == false).ToList();
        }

        public Train Get(int id)
        {
            return _database.Trains.Find(id);
        }

        public IEnumerable<Train> Find(Func<Train, bool> predicate)
        {
            return _database.Trains.Where(t => t.IsDeleted == false).Where(predicate).ToList();
        }

        public void Create(Train item)
        {
            _database.Trains.Add(item);
        }

        public void Update(Train item)
        {
            _database.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var train = _database.Trains.Find(id);
            if (train != null)
            {
                train.IsDeleted = true;
            }
        }
    }
}