﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApp.DAL.EF;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;

namespace WebApp.DAL.Repositories
{
    public class StationsRepository : IRepository<Station>
    {
        private ApplicationContext _database;

        public StationsRepository(ApplicationContext database)
        {
            _database = database;
        }

        public IEnumerable<Station> GetAll()
        {
            return _database.Stations.Include(t => t.RoutesFrom).Include(t => t.RoutesTo).Where(t => t.IsDeleted == false).ToList();
        }

        public Station Get(int id)
        {
            return _database.Stations.Find(id);
        }

        public IEnumerable<Station> Find(Func<Station, bool> predicate)
        {
            return _database.Stations.Include(t => t.RoutesFrom).Include(t => t.RoutesTo).Where(predicate).ToList();
        }

        public void Create(Station item)
        {
            _database.Stations.Add(item);
        }

        public void Update(Station item)
        {
            _database.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var station = _database.Stations.Find(id);
            if (station != null)
            {
                station.IsDeleted = true;
            }
        }
    }
}