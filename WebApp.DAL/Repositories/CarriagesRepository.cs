﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApp.DAL.EF;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;

namespace WebApp.DAL.Repositories
{
    public class CarriagesRepository : IRepository<Carriage>
    {
        private ApplicationContext _database;

        public CarriagesRepository(ApplicationContext database)
        {
            _database = database;
        }

        public IEnumerable<Carriage> GetAll()
        {
            return _database.Carriages.Where(t => t.IsDeleted == false).Include(t => t.Train).ToList();
        }

        public Carriage Get(int id)
        {
            return _database.Carriages.Find(id);
        }

        public IEnumerable<Carriage> Find(Func<Carriage, bool> predicate)
        {
            return _database.Carriages.Where(t => t.IsDeleted == false).Include(t => t.Train).Where(predicate).ToList();
        }

        public void Create(Carriage item)
        {
            _database.Carriages.Add(item);
        }

        public void Update(Carriage item)
        {
            _database.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var carriage = _database.Carriages.Find(id);
            if (carriage != null)
            {
                carriage.IsDeleted = true;
            }
        }
    }
}