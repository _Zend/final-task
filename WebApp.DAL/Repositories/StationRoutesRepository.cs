﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApp.DAL.EF;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;


namespace WebApp.DAL.Repositories
{
    public class StationRoutesRepository : IRepository<StationRoute>
    {
        private ApplicationContext _database;

        public StationRoutesRepository(ApplicationContext database)
        {
            _database = database;
        }

        public IEnumerable<StationRoute> GetAll()
        {
            return _database.StationRoutes.Where(t => t.IsDeleted == false).ToList();
        }

        public StationRoute Get(int id)
        {
            return _database.StationRoutes.Find(id);
        }

        public IEnumerable<StationRoute> Find(Func<StationRoute, bool> predicate)
        {
            return _database.StationRoutes.Where(t => t.IsDeleted == false).Where(predicate).ToList();
        }

        public void Create(StationRoute item)
        {
            _database.StationRoutes.Add(item);
        }

        public void Update(StationRoute item)
        {
            _database.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var stationRoute = _database.StationRoutes.Find(id);
            if (stationRoute != null)
            {
                stationRoute.IsDeleted = true;
            }
        }
    }
}