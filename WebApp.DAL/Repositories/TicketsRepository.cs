﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApp.DAL.EF;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;

namespace WebApp.DAL.Repositories
{
    public class TicketsRepository : IRepository<Ticket>
    {
        private ApplicationContext _database;

        public TicketsRepository(ApplicationContext database)
        {
            _database = database;
        }

        public IEnumerable<Ticket> GetAll()
        {
            return _database.Tickets.Where(t => t.IsDeleted == false).Include(t => t.ApplicationUser).Include(t => t.Carriage).Include(t => t.Seat).Include(t => t.Trip).ToList();
        }

        public Ticket Get(int id)
        {
            return _database.Tickets.Find(id);
        }

        public IEnumerable<Ticket> Find(Func<Ticket, bool> predicate)
        {
            return _database.Tickets.Where(t => t.IsDeleted == false).Include(t => t.ApplicationUser).Include(t => t.Carriage).Include(t => t.Seat).Include(t => t.Trip).Where(predicate).ToList();
        }

        public void Create(Ticket item)
        {
            _database.Tickets.Add(item);
        }

        public void Update(Ticket item)
        {
            _database.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var ticket = _database.Tickets.Find(id);
            if (ticket != null)
            {
                _database.Entry(ticket).State = EntityState.Deleted;
            }
        }
    }
}