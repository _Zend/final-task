﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApp.DAL.EF;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;

namespace WebApp.DAL.Repositories
{
    public class TicketInfosRepository : IRepository<TicketInfo>
    {
        private ApplicationContext _database;

        public TicketInfosRepository(ApplicationContext database)
        {
            _database = database;
        }

        public IEnumerable<TicketInfo> GetAll()
        {
            return _database.TicketInfos.ToList();
        }

        public TicketInfo Get(int id)
        {
            return _database.TicketInfos.Find(id);
        }

        public IEnumerable<TicketInfo> Find(Func<TicketInfo, bool> predicate)
        {
            return _database.TicketInfos.Where(predicate).ToList();
        }

        public void Create(TicketInfo item)
        {
            _database.TicketInfos.Add(item);
        }

        public void Update(TicketInfo item)
        {
            _database.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var ticketInfo = _database.TicketInfos.Find(id);
            if (ticketInfo != null)
            {
                _database.Entry(ticketInfo).State = EntityState.Deleted;
            }
        }
    }
}