﻿using WebApp.DAL.Models.Identity;

namespace WebApp.DAL.Models.Domain
{
    public class Ticket
    {
        public int Id { get; set; }
        public int TripId { get; set; }
        public Trip Trip { get; set; }
        public int CarriageId { get; set; }
        public Carriage Carriage { get; set; }
        public int SeatId { get; set; }
        public Seat Seat { get; set; }
        public double Cost { get; set; }
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public bool IsDeleted { get; set; }
        public Ticket()
        {
            IsDeleted = false;
        }
    }
}