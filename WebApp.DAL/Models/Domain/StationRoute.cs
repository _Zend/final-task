﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.DAL.Models.Domain
{
    public class StationRoute
    {
        public int Id { get; set; }
        public int StationId { get; set; }
        public Station Station { get; set; }
        public int RouteId { get; set; }
        public Route Route { get; set; }
        public int GoingTime { get; set; }
        public int StandingTime { get; set; }
        public int OrderInRoute { get; set; }
        public int TraversingCost { get; set; }
        public bool IsDeleted { get; set; }
    }
}