﻿using System.Collections.Generic;

namespace WebApp.DAL.Models.Domain
{
    public class Station
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsDeleted { get; set; }
        public virtual ICollection<Route> RoutesFrom { get; set; }
        public virtual ICollection<Route> RoutesTo { get; set; }
        public Station()
        {
            IsDeleted = false;
        }
    }
}