﻿namespace WebApp.DAL.Models.Domain
{
    public class Carriage
    {
        public int Id { get; set; }
        public CarriageType Type { get; set; }
        public int TrainId { get; set; }
        public Train Train { get; set; }
        public int Number { get; set; }
        public bool IsDeleted { get; set; }

        public Carriage()
        {
            IsDeleted = false;
        }
    }

    public enum CarriageType
    {
        Reserved = 1,
        Sleeping = 2,
        Compartment = 3
    }
}