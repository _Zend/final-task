﻿namespace WebApp.DAL.Models.Domain
{
    public class Train
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsDeleted { get; set; }
        public Train()
        {
            IsDeleted = false;
        }
    }
}