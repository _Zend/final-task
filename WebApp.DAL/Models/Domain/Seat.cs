﻿namespace WebApp.DAL.Models.Domain
{
    public class Seat
    {
        public int Id { get; set; }
        public int CarriageId { get; set; }
        public Carriage Carriage { get; set; }
        public int Number { get; set; }
        public bool IsDeleted { get; set; }

        public Seat()
        {
            IsDeleted = false;
        }
    }
}