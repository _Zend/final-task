﻿namespace WebApp.DAL.Models.Domain
{
    public class Route
    {
        public int Id { get; set; }
        public int StationIdFrom { get; set; }
        public Station StationFrom { get; set; }
        public int StationIdTo { get; set; }
        public Station StationTo { get; set; }
        public int Cost { get; set; }
        public int TimeInRoute { get; set; }
        public bool IsDeleted { get; set; }

        public Route()
        {
            IsDeleted = false;
        }
    }
}