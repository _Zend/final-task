﻿using System;

namespace WebApp.DAL.Models.Domain
{
    public class Trip
    {
        public int Id { get; set; }
        public int TrainId { get; set; }
        public Train Train { get; set; }
        public int RouteId { get; set; }
        public Route Route { get; set; }
        public DateTime StartDateTime { get; set; }
        public bool IsDeleted { get; set; }

        public Trip()
        {
            IsDeleted = false;
        }
    }
}