﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using WebApp.DAL.Models.Domain;

namespace WebApp.DAL.Models.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public int Year { get; set; }
        public int Bank { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
        public ApplicationUser()
        {
        }
    }
}