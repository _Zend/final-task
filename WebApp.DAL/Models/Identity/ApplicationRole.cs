﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace WebApp.DAL.Models.Identity
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole()
        {
        }
        public string Description { get; set; }
    }
}