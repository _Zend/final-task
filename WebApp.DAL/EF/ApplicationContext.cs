﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using WebApp.DAL.Models.Domain;
using WebApp.DAL.Models.Identity;

namespace WebApp.DAL.EF
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext() : base("IdentityDbExt")
        {
        }

        public DbSet<TicketInfo> TicketInfos { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<StationRoute> StationRoutes { get; set; }
        public DbSet<Train> Trains { get; set; }
        public DbSet<Carriage> Carriages { get; set; }
        public DbSet<Seat> Seats { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<Ticket> Tickets { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Route>()
                .HasRequired(t => t.StationFrom)
                .WithMany(t => t.RoutesFrom)
                .HasForeignKey(t => t.StationIdFrom)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Route>()
                .HasRequired(t => t.StationTo)
                .WithMany(t => t.RoutesTo)
                .HasForeignKey(t => t.StationIdTo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StationRoute>()
                .HasKey(t => t.Id);

            modelBuilder.Entity<Ticket>()
                .HasRequired(t => t.Seat)
                .WithMany()
                .HasForeignKey(t => t.SeatId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Trip>()
                .HasRequired(t => t.Train)
                .WithMany()
                .HasForeignKey(t => t.TrainId)
                .WillCascadeOnDelete(false);
        }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }
    }
}