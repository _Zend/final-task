﻿using Microsoft.AspNet.Identity.EntityFramework;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;
using WebApp.DAL.Models.Identity;
using WebApp.DAL.Repositories;

namespace WebApp.DAL.EF
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationContext _database;
        private CarriagesRepository _carriagesRepository;
        private RoutesRepository _routesRepository;
        private SeatsRepository _seatsRepository;
        private StationRoutesRepository _stationRoutesRepository;
        private StationsRepository _stationsRepository;
        private TicketsRepository _ticketsRepository;
        private TrainsRepository _trainsRepository;
        private TripsRepository _tripsRepository;
        private TicketInfosRepository _ticketInfosRepository;
        private ApplicationUserManager _applicationUserManager;
        private ApplicationRoleManager _applicationRoleManager;

        public ApplicationRoleManager RoleManager
        {
            get { return _applicationRoleManager; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _applicationUserManager; }
        }

        public IRepository<Carriage> CarriagesRepository
        {
            get
            {
                if (_carriagesRepository == null)
                {
                    _carriagesRepository = new CarriagesRepository(_database);
                }
                return _carriagesRepository;
            }
        }

        public IRepository<Route> RoutesRepository
        {
            get
            {
                if (_routesRepository == null)
                {
                    _routesRepository = new RoutesRepository(_database);
                }
                return _routesRepository;
            }
        }

        public IRepository<Seat> SeatsRepository
        {
            get
            {
                if (_seatsRepository == null)
                {
                    _seatsRepository = new SeatsRepository(_database);
                }
                return _seatsRepository;
            }
        }

        public IRepository<StationRoute> StationRoutesRepository
        {
            get
            {
                if (_stationRoutesRepository == null)
                {
                    _stationRoutesRepository = new StationRoutesRepository(_database);
                }
                return _stationRoutesRepository;
            }
        }

        public IRepository<Station> StationsRepository
        {
            get
            {
                if (_stationsRepository == null)
                {
                    _stationsRepository = new StationsRepository(_database);
                }
                return _stationsRepository;
            }
        }

        public IRepository<Ticket> TicketsRepository
        {
            get
            {
                if (_ticketsRepository == null)
                {
                    _ticketsRepository = new TicketsRepository(_database);
                }
                return _ticketsRepository;
            }
        }

        public IRepository<Train> TrainsRepository
        {
            get
            {
                if (_trainsRepository == null)
                {
                    _trainsRepository = new TrainsRepository(_database);
                }
                return _trainsRepository;
            }
        }

        public IRepository<Trip> TripsRepository
        {
            get
            {
                if (_tripsRepository == null)
                {
                    _tripsRepository = new TripsRepository(_database);
                }
                return _tripsRepository;
            }
        }
        public IRepository<TicketInfo> TicketInfosRepository
        {
            get
            {
                if (_ticketInfosRepository == null)
                {
                    _ticketInfosRepository = new TicketInfosRepository(_database);
                }
                return _ticketInfosRepository;
            }
        }

        public void Save()
        {
            _database.SaveChanges();
        }

        public UnitOfWork()
        {
            _database = new ApplicationContext();
            _applicationUserManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_database));
            _applicationRoleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(_database));
        }
    }
}