﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public enum SeatType
    {
        Reserved = 1,
        Sleeping = 2,
        Compartment = 3
    }
}