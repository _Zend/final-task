﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class CarriageViewModel
    {
        public int Id { get; set; }
        public int Number { get; set; }
    }
}