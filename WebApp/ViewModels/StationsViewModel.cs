﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace WebApp.ViewModels
{
    public class StationsViewModel
    {
        [Required]
        public StationViewModel StationFrom { get; set; }
        [Required]
        public StationViewModel StationTo { get; set; }
        [Required(ErrorMessage = "You have to specify a date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }
        public List<SelectListItem> StationsFrom { get; set; }
        public List<SelectListItem> StationsTo { get; set; }
    }
}