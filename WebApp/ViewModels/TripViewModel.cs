﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class TripViewModel
    {
        public int TrainId { get; set; }
        public int TripId { get; set; }
        public string TrainTitle { get; set; }
        public StationInfoViewModel StationFrom { get; set; }
        public StationInfoViewModel StationTo { get; set; }
        public StationInfoViewModel StationBoarding { get; set; }
        public StationInfoViewModel StationArriving { get; set; }
        public DateTime DateTimeOfStart { get; set; }
        public DateTime DateTimeOfArriving { get; set; }
        public TimeSpan TimeInRoute { get; set; }
        public TimeSpan FullTimeInRoute { get; set; }
        public double Cost { get; set; }
        public Dictionary<int, int> FreeSeats { get; set; } // int - type, int - amount
        public Dictionary<int, StationInfoViewModel> TraverseStations { get; set; } // int - order
    }

}