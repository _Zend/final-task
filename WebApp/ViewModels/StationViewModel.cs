﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class StationViewModel
    {
        [Required(ErrorMessage = "You have to specify the station title")]
        public string Title { get; set; }
    }
}