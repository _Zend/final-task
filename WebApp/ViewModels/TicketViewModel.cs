﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.ViewModels
{
    public class TicketViewModel
    {
        public int TripId { get; set; }
        public string TrainTitle { get; set; }
        public int TrainId { get; set; }
        public int CarriageId { get; set; }
        public int userdiscount { get; set; }
        public double Cost { get; set; }
        public string stationboarding { get; set; }
        public string stationarriving { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy mm:hh:ss}")]
        public DateTime arrivingdate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy mm:hh:ss}")]
        public DateTime boardingdate { get; set; }
        public TimeSpan timeinroute { get; set; }
        public SelectList Carriages { get; set; }
    }
}