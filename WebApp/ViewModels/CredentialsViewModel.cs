﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class CredentialsViewModel
    {
        [Required]
        [RegularExpression(@"^[А-Я]{2}\#[0-9]{8}$", ErrorMessage = "Enter correct string. Pattern: AA#00000000.")]
        public string StudentCardNumber { get; set; }
    }
}