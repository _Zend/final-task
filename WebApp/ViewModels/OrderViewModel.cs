﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.ViewModels
{
    public class OrderViewModel
    {
        [Required]
        public int TripId { get; set; }
        [Required]
        public int CarriageId { get; set; }
        [Required]
        public int SeatId { get; set; }
        [Required]
        public double Cost { get; set; }
        [Required]
        public int userdiscount { get; set; }
        public string stationboarding { get; set; }
        public string stationarriving { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [HiddenInput(DisplayValue = false)]
        public DateTime arrivingdate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [HiddenInput(DisplayValue = false)]
        public DateTime boardingdate { get; set; }
        public TimeSpan timeinroute { get; set; }
    }
}