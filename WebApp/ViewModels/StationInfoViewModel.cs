﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class StationInfoViewModel
    {
        public string Title { get; set; }
        public DateTime DateTimeOfStart { get; set; }
        public DateTime DateTimeOfLeaving { get; set; }
        public TimeSpan TimeOfWaiting { get; set; }

    }
}