﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels.Admin
{
    public class StationRouteAdminViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int StationId { get; set; }
        public string StationTitle { get; set; }
        [Required]
        public int RouteId { get; set; }
        [Required]
        public int GoingTime { get; set; }
        [Required]
        public int StandingTime { get; set; }
        [Required]
        public int OrderInRoute { get; set; }
        [Required]
        public int TraversingCost { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
    }
}