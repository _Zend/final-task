﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels.Admin
{
    public class SeatAdminViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int CarriageId { get; set; }
        [Required]
        public int Number { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
    }
}