﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels.Admin
{
    public class RouteAdminViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int StationIdFrom { get; set; }
        [Required]
        public int StationIdTo { get; set; }
        [Required]
        public int Cost { get; set; }
        [Required]
        public int TimeInRoute { get; set; }
        public string StationFrom { get; set; }
        public string StationTo { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
    }
}