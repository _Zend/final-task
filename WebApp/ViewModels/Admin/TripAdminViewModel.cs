﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels.Admin
{
    public class TripAdminViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int TrainId { get; set; }
        [Required]
        public int RouteId { get; set; }
        [Required]
        public DateTime StartDateTime { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
    }
}