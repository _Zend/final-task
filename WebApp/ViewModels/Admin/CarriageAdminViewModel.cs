﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebApp.DAL.Models.Domain;

namespace WebApp.ViewModels.Admin
{
    public class CarriageAdminViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public CarriageType Type { get; set; }
        [Required]
        public int TrainId { get; set; }
        [Required]
        public int Number { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
    }
}