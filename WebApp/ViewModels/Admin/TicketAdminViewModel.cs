﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels.Admin
{
    public class TicketAdminViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int TripId { get; set; }
        [Required]
        public int CarriageId { get; set; }
        [Required]
        public int SeatId { get; set; }
        [Required]
        public double Cost { get; set; }
        [Required]
        public string ApplicationUserId { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
    }
}