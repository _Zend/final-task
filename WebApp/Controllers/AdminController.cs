﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using WebApp.BLL.DTO.Admin;
using WebApp.BLL.Infrastructure;
using WebApp.BLL.Services;
using WebApp.ViewModels.Admin;

namespace WebApp.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private AdminService AdminService { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Stations()
        {
            var stationAdminDTOs = AdminService.GetStations();
            Mapper.Initialize(cfg => cfg.CreateMap<StationAdminDTO, StationAdminViewModel>());
            var stationAdminViewModels = Mapper.Map<List<StationAdminViewModel>>(stationAdminDTOs);
            return View(stationAdminViewModels);

        }

        [HttpGet]
        public ActionResult CreateStation()
        {
            return View(new StationAdminViewModel());
        }

        [HttpPost]
        public ActionResult CreateStation(StationAdminViewModel stationAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("CreateStation");
            Mapper.Initialize(cfg => cfg.CreateMap<StationAdminViewModel, StationAdminDTO>());
            var stationAdminDTO = Mapper.Map<StationAdminDTO>(stationAdminViewModel);
            try
            {
                AdminService.CreateStation(stationAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("CreateStation");
            }

            return RedirectToAction("Stations");
        }

        [HttpGet]
        public ActionResult EditStation(int id)
        {
            var stationAdminDTO = AdminService.GetStation(id);
            Mapper.Initialize(cfg => cfg.CreateMap<StationAdminDTO, StationAdminViewModel>());
            var stationAdminViewModel = Mapper.Map<StationAdminViewModel>(stationAdminDTO);
            return View(stationAdminViewModel);
        }

        [HttpPost]
        public ActionResult EditStation(StationAdminViewModel stationAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("EditStation", stationAdminViewModel);
            Mapper.Initialize(cfg => cfg.CreateMap<StationAdminViewModel, StationAdminDTO>());
            var stationAdminDTO = Mapper.Map<StationAdminDTO>(stationAdminViewModel);
            try
            {
                AdminService.UpdateStation(stationAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("EditStation");
            }

            return RedirectToAction("Stations");
        }

        [HttpGet]
        public ActionResult DeleteStation(int id)
        {
            AdminService.DeleteStation(id);

            var redirecturl = "";
            try
            {
                redirecturl = Request.UrlReferrer.ToString();
            }
            catch (NullReferenceException)
            {
                redirecturl = "/";
            }

            return Redirect(redirecturl);
        }

        public ActionResult Routes()
        {
            var routeAdminDTOs = AdminService.GetRoutes();
            Mapper.Initialize(cfg => cfg.CreateMap<RouteAdminDTO, RouteAdminViewModel>());
            var routeAdminViewModels = Mapper.Map<List<RouteAdminViewModel>>(routeAdminDTOs);
            return View(routeAdminViewModels);
        }

        [HttpGet]
        public ActionResult CreateRoute()
        {
            return View(new RouteAdminViewModel());
        }

        [HttpPost]
        public ActionResult CreateRoute(RouteAdminViewModel routeAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("CreateRoute");
            Mapper.Initialize(cfg => cfg.CreateMap<RouteAdminViewModel, RouteAdminDTO>());
            var routeAdminDTO = Mapper.Map<RouteAdminDTO>(routeAdminViewModel);
            try
            {
                AdminService.CreateRoute(routeAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("CreateRoute");
            }

            return RedirectToAction("Routes");
        }

        [HttpGet]
        public ActionResult EditRoute(int id)
        {
            var routeAdminDTO = AdminService.GetRoute(id);
            Mapper.Initialize(cfg => cfg.CreateMap<RouteAdminDTO, RouteAdminViewModel>());
            var routeAdminViewModel = Mapper.Map<RouteAdminViewModel>(routeAdminDTO);
            return View(routeAdminViewModel);
        }           

        [HttpPost]
        public ActionResult EditRoute(RouteAdminViewModel routeAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("EditRoute", routeAdminViewModel);
            Mapper.Initialize(cfg => cfg.CreateMap<RouteAdminViewModel, RouteAdminDTO>());
            var routeAdminDTO = Mapper.Map<RouteAdminDTO>(routeAdminViewModel);

            try
            {
                AdminService.UpdateRoute(routeAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("EditRoute");
            }

            return RedirectToAction("Routes");
        }

        [HttpGet]
        public ActionResult DeleteRoute(int id)
        {
            AdminService.DeleteRoute(id);

            var redirecturl = "";
            try
            {
                redirecturl = Request.UrlReferrer.ToString();
            }
            catch (NullReferenceException)
            {
                redirecturl = "/";
            }

            return Redirect(redirecturl);
        }

        public ActionResult StationRoutes(int routeId)
        {
            var stationRoutesDTOs = AdminService.GetStationRoutesForRoute(routeId);
            Mapper.Initialize(cfg => cfg.CreateMap<StationRouteAdminDTO, StationRouteAdminViewModel>());
            var stationrouteAdminViewModels = Mapper.Map<List<StationRouteAdminViewModel>>(stationRoutesDTOs);

            return View(stationrouteAdminViewModels);
        }

        [HttpGet]
        public ActionResult CreateStationRoute(int routeId)
        {
            return View(new StationRouteAdminViewModel() {RouteId = routeId});
        }

        [HttpPost]
        public ActionResult CreateStationRoute(StationRouteAdminViewModel stationrouteAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("CreateStationRoute", stationrouteAdminViewModel);
            Mapper.Initialize(cfg => cfg.CreateMap<StationRouteAdminViewModel, StationRouteAdminDTO>());
            var stationrouteAdminDTO = Mapper.Map<StationRouteAdminDTO>(stationrouteAdminViewModel);
            try
            {
                AdminService.CreateStationRoute(stationrouteAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("CreateStationRoute");
            }

            return RedirectToAction("StationRoutes", new {routeId = stationrouteAdminViewModel.RouteId});
        }

        [HttpGet]
        public ActionResult EditStationRoute(int id)
        {
            var stationrouteAdminDTO = AdminService.GetStationRoute(id);
            Mapper.Initialize(cfg => cfg.CreateMap<StationRouteAdminDTO, StationRouteAdminViewModel>());
            var stationrouteAdminViewModel = Mapper.Map<StationRouteAdminViewModel>(stationrouteAdminDTO);
            return View(stationrouteAdminViewModel);
        }

        [HttpPost]
        public ActionResult EditStationRoute(StationRouteAdminViewModel stationrouteAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("EditStationRoute", stationrouteAdminViewModel);
            Mapper.Initialize(cfg => cfg.CreateMap<StationRouteAdminViewModel, StationRouteAdminDTO>());
            var stationrouteAdminDTO = Mapper.Map<StationRouteAdminDTO>(stationrouteAdminViewModel);

            try
            {
                AdminService.UpdateStationRoute(stationrouteAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("EditStationRoute");
            }

            return RedirectToAction("StationRoutes", new {routeId = stationrouteAdminViewModel.RouteId});
        }

        [HttpGet]
        public ActionResult DeleteStationRoute(int id)
        {
            AdminService.DeleteStationRoute(id);

            var redirecturl = "";
            try
            {
                redirecturl = Request.UrlReferrer.ToString();
            }
            catch (NullReferenceException)
            {
                redirecturl = "/";
            }

            return Redirect(redirecturl);
        }

        public ActionResult Trains()
        {
            var trainsAdminDTOs = AdminService.GetTrains();
            Mapper.Initialize(cfg => cfg.CreateMap<TrainAdminDTO, TrainAdminViewModel>());
            var trainAdminViewModels = Mapper.Map<List<TrainAdminViewModel>>(trainsAdminDTOs);

            return View(trainAdminViewModels);
        }

        [HttpGet]
        public ActionResult CreateTrain()
        {
            return View(new TrainAdminViewModel());
        }

        [HttpPost]
        public ActionResult CreateTrain(TrainAdminViewModel trainAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("CreateTrain");
            Mapper.Initialize(cfg => cfg.CreateMap<TrainAdminViewModel, TrainAdminDTO>());
            var trainAdminDTO = Mapper.Map<TrainAdminDTO>(trainAdminViewModel);

            try
            {
                AdminService.CreateTrain(trainAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("CreateTrain");
            }

            return RedirectToAction("Trains");
        }

        [HttpGet]
        public ActionResult EditTrain(int id)
        {
            var trainAdminDTO = AdminService.GetTrain(id);
            Mapper.Initialize(cfg => cfg.CreateMap<TrainAdminDTO, TrainAdminViewModel>());
            var trainAdminViewModel = Mapper.Map<TrainAdminViewModel>(trainAdminDTO);
            return View(trainAdminViewModel);
        }

        [HttpPost]
        public ActionResult EditTrain(TrainAdminViewModel trainAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("EditTrain", trainAdminViewModel);
            Mapper.Initialize(cfg => cfg.CreateMap<TrainAdminViewModel, TrainAdminDTO>());
            var trainAdminDTO = Mapper.Map<TrainAdminDTO>(trainAdminViewModel);

            try
            {
                AdminService.UpdateTrain(trainAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("EditTrain");
            }

            return RedirectToAction("Trains");
        }

        [HttpGet]
        public ActionResult DeleteTrain(int id)
        {
            AdminService.DeleteTrain(id);

            var redirecturl = "";
            try
            {
                redirecturl = Request.UrlReferrer.ToString();
            }
            catch (NullReferenceException)
            {
                redirecturl = "/";
            }

            return Redirect(redirecturl);
        }

        public ActionResult Carriages(int trainId)
        {
            var carriagesDTOs = AdminService.GetCarriagesForTrain(trainId);
            Mapper.Initialize(cfg => cfg.CreateMap<CarriageAdminDTO, CarriageAdminViewModel>());
            var carriageAdminViewModels = Mapper.Map<List<CarriageAdminViewModel>>(carriagesDTOs);

            return View(carriageAdminViewModels);
        }

        [HttpGet]
        public ActionResult CreateCarriage(int trainId)
        {
            return View(new CarriageAdminViewModel() {TrainId = trainId});
        }

        [HttpPost]
        public ActionResult CreateCarriage(CarriageAdminViewModel carriageAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("CreateCarriage", carriageAdminViewModel);
            Mapper.Initialize(cfg => cfg.CreateMap<CarriageAdminViewModel, CarriageAdminDTO>());
            var carriageAdminDTO = Mapper.Map<CarriageAdminDTO>(carriageAdminViewModel);

            try
            {
                AdminService.CreateCarriage(carriageAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("CreateCarriage");
            }

            return RedirectToAction("Carriages", new {trainId = carriageAdminViewModel.TrainId});
        }

        [HttpGet]
        public ActionResult EditCarriage(int id)
        {
            var carriageAdminDTO = AdminService.GetCarriage(id);
            Mapper.Initialize(cfg => cfg.CreateMap<CarriageAdminDTO, CarriageAdminViewModel>());
            var carriageAdminViewModel = Mapper.Map<CarriageAdminViewModel>(carriageAdminDTO);
            return View(carriageAdminViewModel);
        }

        [HttpPost]
        public ActionResult EditCarriage(CarriageAdminViewModel carriageAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("EditCarriage", carriageAdminViewModel);
            Mapper.Initialize(cfg => cfg.CreateMap<CarriageAdminViewModel, CarriageAdminDTO>());
            var carriageAdminDTO = Mapper.Map<CarriageAdminDTO>(carriageAdminViewModel);

            try
            {
                AdminService.UpdateCarriage(carriageAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("EditCarriage");
            }

            return RedirectToAction("Carriages", new {trainId = carriageAdminViewModel.TrainId});
        }

        [HttpGet]
        public ActionResult DeleteCarriage(int id)
        {
            AdminService.DeleteCarriage(id);

            var redirecturl = "";
            try
            {
                redirecturl = Request.UrlReferrer.ToString();
            }
            catch (NullReferenceException)
            {
                redirecturl = "/";
            }

            return Redirect(redirecturl);
        }

        public ActionResult Seats(int carriageId)
        {
            var seatsDTOs = AdminService.GetSeatsForCarriage(carriageId);
            Mapper.Initialize(cfg => cfg.CreateMap<SeatAdminDTO, SeatAdminViewModel>());
            var seatAdminViewModels = Mapper.Map<List<SeatAdminViewModel>>(seatsDTOs);

            return View(seatAdminViewModels);
        }

        [HttpGet]
        public ActionResult CreateSeat(int carriageId)
        {
            return View(new SeatAdminViewModel() {CarriageId = carriageId});
        }

        [HttpPost]
        public ActionResult CreateSeat(SeatAdminViewModel seatAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("CreateSeat", seatAdminViewModel);
            Mapper.Initialize(cfg => cfg.CreateMap<SeatAdminViewModel, SeatAdminDTO>());
            var seatAdminDTO = Mapper.Map<SeatAdminDTO>(seatAdminViewModel);

            try
            {
                AdminService.CreateSeat(seatAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("CreateSeat");
            }

            return RedirectToAction("Seats", new {carriageId = seatAdminViewModel.CarriageId});
        }

        [HttpGet]
        public ActionResult EditSeat(int id)
        {
            var seatAdminDTO = AdminService.GetSeat(id);
            Mapper.Initialize(cfg => cfg.CreateMap<SeatAdminDTO, SeatAdminViewModel>());
            var seatAdminViewModel = Mapper.Map<SeatAdminViewModel>(seatAdminDTO);
            return View(seatAdminViewModel);
        }

        [HttpPost]
        public ActionResult EditSeat(SeatAdminViewModel seatAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("EditSeat", seatAdminViewModel);
            Mapper.Initialize(cfg => cfg.CreateMap<SeatAdminViewModel, SeatAdminDTO>());
            var seatAdminDTO = Mapper.Map<SeatAdminDTO>(seatAdminViewModel);

            try
            {
                AdminService.UpdateSeat(seatAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("EditSeat");
            }

            return RedirectToAction("Seats", new {carriageId = seatAdminViewModel.CarriageId});
        }

        [HttpGet]
        public ActionResult DeleteSeat(int id)
        {
            AdminService.DeleteSeat(id);

            var redirecturl = "";
            try
            {
                redirecturl = Request.UrlReferrer.ToString();
            }
            catch (NullReferenceException)
            {
                redirecturl = "/";
            }

            return Redirect(redirecturl);
        }

        public ActionResult Trips()
        {
            var tripAdminDTOs = AdminService.GetTrips();
            Mapper.Initialize(cfg => cfg.CreateMap<TripAdminDTO, TripAdminViewModel>());
            var tripAdminViewModels = Mapper.Map<List<TripAdminViewModel>>(tripAdminDTOs);
            return View(tripAdminViewModels);
        }

        [HttpGet]
        public ActionResult CreateTrip()
        {
            return View(new TripAdminViewModel());
        }

        [HttpPost]
        public ActionResult CreateTrip(TripAdminViewModel tripAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("CreateTrip");
            Mapper.Initialize(cfg => cfg.CreateMap<TripAdminViewModel, TripAdminDTO>());
            var tripAdminDTO = Mapper.Map<TripAdminDTO>(tripAdminViewModel);

            try
            {
                AdminService.CreateTrip(tripAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("CreateTrip");
            }

            return RedirectToAction("Trips");
        }

        [HttpGet]
        public ActionResult EditTrip(int id)
        {
            var tripAdminDTO = AdminService.GetTrip(id);
            Mapper.Initialize(cfg => cfg.CreateMap<TripAdminDTO, TripAdminViewModel>());
            var tripAdminViewModel = Mapper.Map<TripAdminViewModel>(tripAdminDTO);
            return View(tripAdminViewModel);
        }

        [HttpPost]
        public ActionResult EditTrip(TripAdminViewModel tripAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("EditTrip", tripAdminViewModel);
            Mapper.Initialize(cfg => cfg.CreateMap<TripAdminViewModel, TripAdminDTO>());
            var tripAdminDTO = Mapper.Map<TripAdminDTO>(tripAdminViewModel);

            try
            {
                AdminService.UpdateTrip(tripAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("EditTrip");
            }

            return RedirectToAction("Trips");
        }

        [HttpGet]
        public ActionResult DeleteTrip(int id)
        {
            AdminService.DeleteTrip(id);

            var redirecturl = "";
            try
            {
                redirecturl = Request.UrlReferrer.ToString();
            }
            catch (NullReferenceException)
            {
                redirecturl = "/";
            }

            return Redirect(redirecturl);
        }

        public ActionResult Tickets()
        {
            var ticketAdminDTOs = AdminService.GetTickets();
            Mapper.Initialize(cfg => cfg.CreateMap<TicketAdminDTO, TicketAdminViewModel>());
            var ticketAdminViewModels = Mapper.Map<List<TicketAdminViewModel>>(ticketAdminDTOs);
            return View(ticketAdminViewModels);
        }

        [HttpGet]
        public ActionResult CreateTicket()
        {
            return View(new TicketAdminViewModel());
        }

        [HttpPost]
        public ActionResult CreateTicket(TicketAdminViewModel ticketAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("CreateTicket");
            Mapper.Initialize(cfg => cfg.CreateMap<TicketAdminViewModel, TicketAdminDTO>());
            var ticketAdminDTO = Mapper.Map<TicketAdminDTO>(ticketAdminViewModel);

            try
            {
                AdminService.CreateTicket(ticketAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("CreateTicket");
            }

            return RedirectToAction("Tickets");
        }

        [HttpGet]
        public ActionResult EditTicket(int id)
        {
            var ticketAdminDTO = AdminService.GetTicket(id);
            Mapper.Initialize(cfg => cfg.CreateMap<TicketAdminDTO, TicketAdminViewModel>());
            var ticketAdminViewModel = Mapper.Map<TicketAdminViewModel>(ticketAdminDTO);
            return View(ticketAdminViewModel);
        }

        [HttpPost]
        public ActionResult EditTicket(TicketAdminViewModel ticketAdminViewModel)
        {
            if (!ModelState.IsValid)
                return View("EditTicket", ticketAdminViewModel);
            Mapper.Initialize(cfg => cfg.CreateMap<TicketAdminViewModel, TicketAdminDTO>());
            var ticketAdminDTO = Mapper.Map<TicketAdminDTO>(ticketAdminViewModel);

            try
            {
                AdminService.UpdateTicket(ticketAdminDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View("EditTicket");
            }

            return RedirectToAction("Tickets");
        }

        [HttpGet]
        public ActionResult DeleteTicket(int id)
        {
            AdminService.DeleteTicket(id);

            var redirecturl = "";
            try
            {
                redirecturl = Request.UrlReferrer.ToString();
            }
            catch (NullReferenceException)
            {
                redirecturl = "/";
            }

            return Redirect(redirecturl);
        }


        public AdminController()
        {
            AdminService = new AdminService();
        }
    }
}