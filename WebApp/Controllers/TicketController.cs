﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApp.BLL.Services;
using WebApp.ViewModels;
using AutoMapper;
using WebApp.BLL.DTO;
using WebApp.BLL.Infrastructure;


namespace WebApp.Controllers
{
    [Authorize]
    public class TicketController : Controller
    {
        private TicketService TicketService { get; set; }
        private TrainService TrainService { get; set; }

        [HttpGet]
        public ActionResult Buy(int tripId, double cost, string stationboarding, string stationarriving, string arrivingdate, string boardingdate, TimeSpan timeinroute, string error, int? userdiscount)
        {
            var train = TrainService.GetTrainByTripId(tripId);
            var mapper = new MapperConfiguration(t => t.CreateMap<CarriageDTO, CarriageViewModel>()).CreateMapper();
            var carriages = mapper.Map<List<CarriageViewModel>>(TicketService.GetCarriages(train.Id));
            var carriagesSelectList = new SelectList(carriages, "Id", "Number");

            if (userdiscount == null)
            {
                userdiscount = 0;
            }

            if (User.IsInRole("special"))
            {
                userdiscount = (int) cost / 2;
            }
            
            var ticketViewModel = new TicketViewModel();
            if (error?.Length > 0)
            {
                ModelState.AddModelError("", error);
            }
            ticketViewModel.TripId = tripId;
            ticketViewModel.Carriages = carriagesSelectList;
            ticketViewModel.TrainId = train.Id;
            ticketViewModel.TrainTitle = train.Title;
            ticketViewModel.CarriageId = 1;
            ticketViewModel.Cost = cost;
            ticketViewModel.userdiscount = (int) userdiscount;
            ticketViewModel.stationboarding = stationboarding;
            ticketViewModel.stationarriving = stationarriving;
            ticketViewModel.arrivingdate = DateTime.Parse(arrivingdate, CultureInfo.GetCultureInfo("ru-RU"));
            ticketViewModel.boardingdate = DateTime.Parse(boardingdate, CultureInfo.GetCultureInfo("ru-RU"));
            ticketViewModel.timeinroute = timeinroute;

            return View(ticketViewModel);
        }
        [HttpPost]
        [ActionName("Buy")]
        public ActionResult Buy(OrderViewModel orderViewModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Buy", new {
                    tripId = orderViewModel.TripId,
                    cost = orderViewModel.Cost,
                    userdiscount = orderViewModel.userdiscount,
                    stationboarding = orderViewModel.stationboarding,
                    stationarriving = orderViewModel.stationarriving,
                    arrivingdate = orderViewModel.arrivingdate.ToString("dd/MM/yyyy HH:mm:ss"),
                    boardingdate = orderViewModel.boardingdate.ToString("dd/MM/yyyy HH:mm:ss"),
                    timeinroute = orderViewModel.timeinroute,
                    error = "Invalid values."
                });
            }

            var mapper = new MapperConfiguration(t => t.CreateMap<OrderViewModel, OrderDTO>()).CreateMapper();
            var orderDTO = mapper.Map<OrderDTO>(orderViewModel);
            orderDTO.UserName = User.Identity.Name;
            var ticketInfoId = 0;

            try
            {
                ticketInfoId = TicketService.Buy(orderDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return RedirectToAction("Buy", new
                {
                    tripId = orderViewModel.TripId,
                    cost = orderViewModel.Cost,
                    userdiscount = orderViewModel.userdiscount,
                    stationboarding = orderViewModel.stationboarding,
                    stationarriving = orderViewModel.stationarriving,
                    arrivingdate = orderViewModel.arrivingdate.ToString("dd/MM/yyyy HH:mm:ss"),
                    boardingdate = orderViewModel.boardingdate.ToString("dd/MM/yyyy HH:mm:ss"),
                    timeinroute = orderViewModel.timeinroute,
                    error = e.Message
                });
            }
            catch (BankException b)
            {
                ModelState.AddModelError("", b.Message);
                return RedirectToAction("Buy", new
                {
                    tripId = orderViewModel.TripId,
                    cost = orderViewModel.Cost,
                    userdiscount = orderViewModel.userdiscount,
                    stationboarding = orderViewModel.stationboarding,
                    stationarriving = orderViewModel.stationarriving,
                    arrivingdate = orderViewModel.arrivingdate.ToString("dd/MM/yyyy HH:mm:ss"),
                    boardingdate = orderViewModel.boardingdate.ToString("dd/MM/yyyy HH:mm:ss"),
                    timeinroute = orderViewModel.timeinroute,
                    error = b.Message
                });
            }
            
            return RedirectToAction("Get", new { id = ticketInfoId });
        }

        public ActionResult Get(int id)
        {
            var ticketInfoDTO = TicketService.GetTicket(id);
            var mapper = new MapperConfiguration(t => t.CreateMap<TicketInfoDTO, OrderedTicketViewModel>()).CreateMapper();
            var orderedTicketViewModel = mapper.Map<OrderedTicketViewModel>(ticketInfoDTO);

            return View(orderedTicketViewModel);
        }

        public ActionResult Tickets()
        {
            var ticketInfoDTOs = TicketService.GetTickets(User.Identity.Name);
            var mapper = new MapperConfiguration(t => t.CreateMap<TicketInfoDTO, OrderedTicketViewModel>()).CreateMapper();
            var orderedTicketViewModels = mapper.Map<List<OrderedTicketViewModel>>(ticketInfoDTOs);

            return View(orderedTicketViewModels);
        }

        [HttpPost]
        public ActionResult GetSeats(int id)
        {
            var seats = new SelectList(TicketService.GetSeats(id), "Id", "Number");

            return PartialView("PartialSeats", seats);
        }

        public TicketController()
        {
            this.TicketService = new TicketService();
            this.TrainService = new TrainService();
        }
    }
}