﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.BLL.Services;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    public class DiscountController : Controller
    {
        private DiscountService DiscountService { get; set; }

        public ActionResult Get()
        {
            if (User.IsInRole("special"))
            {
                return RedirectToAction("Index", "Main");
            }

            return View(new CredentialsViewModel());
        }

        [HttpPost]
        public ActionResult Process(CredentialsViewModel credentialsViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("Get");
            }

            DiscountService.AddUserToSpecialGroup(User.Identity.Name);

            return RedirectToAction("Index", "Main");
        }

        public DiscountController()
        {
            DiscountService = new DiscountService();
        }
    }
}