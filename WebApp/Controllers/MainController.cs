﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.ViewModels;
using AutoMapper;
using WebApp.BLL.DTO;
using WebApp.BLL.Infrastructure;
using WebApp.BLL.Services;

namespace WebApp.Controllers
{
    public class MainController : Controller
    {
        private TrainService TrainService { get; set; }

        public ActionResult Index()
        {
            var stationDTOs = TrainService.GetStations();
            var stationsFrom = new List<SelectListItem>();
            var stationsTo = new List<SelectListItem>();

            foreach (var station in stationDTOs)
            {
                stationsFrom.Add(new SelectListItem {Text = station.Title, Value = station.Title});
                stationsTo.Add(new SelectListItem {Text = station.Title, Value = station.Title});
            }

            var stations = new StationsViewModel()
            {
                StationFrom = new StationViewModel(),
                StationTo = new StationViewModel(),
                StationsFrom = stationsFrom,
                StationsTo = stationsTo,
                Date = DateTime.UtcNow
            };

            return View(stations);
        }

        [HttpPost]
        public ActionResult Find(StationsViewModel stations)
        {
            if (stations.StationFrom.Title == stations.StationTo.Title)
            {
                ModelState.AddModelError("", @"Stations must be different.");
            }
            
            if (!ModelState.IsValid)
            {
                var stationDTOs = TrainService.GetStations();
                var stationsFrom = new List<SelectListItem>();
                var stationsTo = new List<SelectListItem>();

                foreach (var station in stationDTOs)
                {
                    stationsFrom.Add(new SelectListItem {Text = station.Title, Value = station.Title});
                    stationsTo.Add(new SelectListItem {Text = station.Title, Value = station.Title});
                }

                stations.StationsFrom = stationsFrom;
                stations.StationsTo = stationsTo;

                return View("Index", stations);
            }

            return RedirectToAction("Trains", new
            {
                stationFrom = stations.StationFrom.Title,
                stationTo = stations.StationTo.Title,
                date = stations.Date.ToShortDateString()
            });
        }
        [HttpGet]
        public ActionResult Trains(string stationFrom, string stationTo, string date)
        {
            var stations = new StationsViewModel()
            {
                StationFrom = new StationViewModel() {Title = stationFrom},
                StationTo = new StationViewModel() {Title = stationTo},
                Date = DateTime.Parse(date)
            };

            Mapper.Initialize(cfg => cfg.CreateMap<StationViewModel, StationDTO>());

            var stFrom = Mapper.Map<StationViewModel, StationDTO>(stations.StationFrom);
            var stTo = Mapper.Map<StationViewModel, StationDTO>(stations.StationTo);

            var stationsDto = new StationsDTO()
            {
                Date = stations.Date,
                StationFrom = stFrom,
                StationTo = stTo
            };

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TripDTO, TripViewModel>();
                cfg.CreateMap<StationInfoDTO, StationInfoViewModel>();
            });

            config.AssertConfigurationIsValid();

            var mapper = config.CreateMapper();
            var tripsDTO = new List<TripDTO>();
            var tripsViewModel = new List<TripViewModel>();

            try
            {
                tripsDTO = TrainService.Find(stationsDto);
                tripsViewModel = mapper.Map<List<TripDTO>, List<TripViewModel>>(tripsDTO);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View(tripsViewModel);
            }

            return View(tripsViewModel);
        }

        public MainController()
        {
            this.TrainService = new TrainService();
        }
    }
}