﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebApp.BLL.DTO.Admin;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Ninject;
using WebApp.BLL.Infrastructure;
using WebApp.BLL.Util;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;

namespace WebApp.BLL.Services
{
    /// <summary>
    /// AdminService is working with "Admin Panel" allowing administrators manage the system's database.
    /// </summary>
    public class AdminService
    {
        protected IUnitOfWork Database { get; }

        public AdminService(IUnitOfWork uow)
        {
            Database = uow;
        }
        public AdminService()
        {
            var kernel = new NinjectDependencyResolver(new StandardKernel());
            Database = kernel.GetService<IUnitOfWork>();
        }
        /// <summary>
        /// Gets a list of all stations.
        /// </summary>
        /// <returns></returns>
        public List<StationAdminDTO> GetStations()
        {
            var stations = Database.StationsRepository.GetAll().ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<Station, StationAdminDTO>());
            var stationAdminDTOs = Mapper.Map<List<StationAdminDTO>>(stations);

            return stationAdminDTOs;
        }
        /// <summary>
        /// Gets a station by it's id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public StationAdminDTO GetStation(int id)
        {
            var station = Database.StationsRepository.Get(id);
            Mapper.Initialize(cfg => cfg.CreateMap<Station, StationAdminDTO>());
            var stationAdminDTO = Mapper.Map<StationAdminDTO>(station);

            return stationAdminDTO;
        }
        /// <summary>
        /// Updates a specified station in the database.
        /// </summary>
        /// <param name="stationAdminDTO"></param>
        public void UpdateStation(StationAdminDTO stationAdminDTO)
        {
            if (Database.StationsRepository.Get(stationAdminDTO.Id) == null)
            {
                throw new ValidationException("Invalid identificator.", "");
            }
            if (Database.StationsRepository.Find(t => t.Title == stationAdminDTO.Title).Any())
            {
                throw new ValidationException("The station you specified already exists in the database.", "");
            }
            var station = Database.StationsRepository.Get(stationAdminDTO.Id);
            Mapper.Initialize(cfg => cfg.CreateMap<StationAdminDTO, Station>().ForMember(t => t.Id, y => y.Ignore()));
            station = Mapper.Map(stationAdminDTO, station);
            Database.StationsRepository.Update(station);
            Database.Save();
        }
        /// <summary>
        /// Creates new station in the database.
        /// </summary>
        /// <param name="stationAdminDTO"></param>
        public void CreateStation(StationAdminDTO stationAdminDTO)
        {
            if (Database.StationsRepository.Find(t => t.Title == stationAdminDTO.Title).Any())
            {
                throw new ValidationException("The station you specified already exists in the database.","");
            }
            Mapper.Initialize(cfg => cfg.CreateMap<StationAdminDTO, Station>().ForMember(t => t.Id, y => y.Ignore()));
            var station = Mapper.Map<Station>(stationAdminDTO);
            Database.StationsRepository.Create(station);
            Database.Save();
        }
        /// <summary>
        /// Deletes a station from the database.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteStation(int id)
        {
            Database.StationsRepository.Delete(id);
            Database.Save();
        }
        /// <summary>
        /// Gets a list of all routes.
        /// </summary>
        /// <returns></returns>
        public List<RouteAdminDTO> GetRoutes()
        {
            var routes = Database.RoutesRepository.GetAll().ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<Route, RouteAdminDTO>());
            var routeAdminDTOs = Mapper.Map<List<RouteAdminDTO>>(routes);

            foreach(var route in routeAdminDTOs)
            {
                route.StationFrom = Database.StationsRepository.Get(route.StationIdFrom).Title;
                route.StationTo = Database.StationsRepository.Get(route.StationIdTo).Title;
            }

            return routeAdminDTOs;
        }
        /// <summary>
        /// Gets a route by it's id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RouteAdminDTO GetRoute(int id)
        {
            var route = Database.RoutesRepository.Get(id);
            Mapper.Initialize(cfg => cfg.CreateMap<Route, RouteAdminDTO>());
            var routeAdminDTO = Mapper.Map<RouteAdminDTO>(route);

            return routeAdminDTO;
        }
        /// <summary>
        /// Updates a specified route in the database.
        /// </summary>
        /// <param name="routeAdminDTO"></param>
        public void UpdateRoute(RouteAdminDTO routeAdminDTO)
        {
            if (Database.RoutesRepository.Get(routeAdminDTO.Id) == null)
            {
                throw new ValidationException("Invalid identificator.", "");
            }
            if (routeAdminDTO.Cost < 0)
            {
                throw new ValidationException("The cost can not be negative.", "");
            }
            if (routeAdminDTO.TimeInRoute < 0)
            {
                throw new ValidationException("The time can not be negative.", "");
            }
            if (Database.StationsRepository.Get(routeAdminDTO.StationIdFrom) == null)
            {
                throw new ValidationException("Invalid starting station.", "");
            }
            if (Database.StationsRepository.Get(routeAdminDTO.StationIdTo) == null)
            {
                throw new ValidationException("Invalid arriving station.", "");
            }
            var route = Database.RoutesRepository.Get(routeAdminDTO.Id);
            Mapper.Initialize(cfg => cfg.CreateMap<RouteAdminDTO, Route>().ForMember(t => t.Id, y => y.Ignore())
            .ForMember(t => t.StationFrom, y => y.Ignore())
            .ForMember(t => t.StationTo, y => y.Ignore()));
            route = Mapper.Map(routeAdminDTO, route);
            Database.RoutesRepository.Update(route);
            Database.Save();
        }
        /// <summary>
        /// Creates new route in the database.
        /// </summary>
        /// <param name="routeAdminDTO"></param>
        public void CreateRoute(RouteAdminDTO routeAdminDTO)
        {
            if (routeAdminDTO.Cost < 0)
            {
                throw new ValidationException("The cost can not be negative.", "");
            }
            if (routeAdminDTO.TimeInRoute < 0)
            {
                throw new ValidationException("The time can not be negative.", "");
            }
            if (Database.StationsRepository.Get(routeAdminDTO.StationIdFrom) == null)
            {
                throw new ValidationException("Invalid starting station.", "");
            }
            if (Database.StationsRepository.Get(routeAdminDTO.StationIdTo) == null)
            {
                throw new ValidationException("Invalid arriving station.", "");
            }
            Mapper.Initialize(cfg => cfg.CreateMap<RouteAdminDTO, Route>().ForMember(t => t.Id, y => y.Ignore())
            .ForMember(t => t.StationFrom, y => y.Ignore())
            .ForMember(t => t.StationTo, y => y.Ignore()));
            var route = Mapper.Map<Route>(routeAdminDTO);
            Database.RoutesRepository.Create(route);
            Database.Save();
        }
        /// <summary>
        /// Deletes a route from the database.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteRoute(int id)
        {
            Database.RoutesRepository.Delete(id);
            Database.Save();
        }
        /// <summary>
        /// Gets a list of all intermediate stations between a route.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<StationRouteAdminDTO> GetStationRoutesForRoute(int id)
        {
            var stationroutes = Database.StationRoutesRepository.Find(t => t.RouteId == id).ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<StationRoute, StationRouteAdminDTO>());
            var stationrouteAdminDTOs = Mapper.Map<List<StationRouteAdminDTO>>(stationroutes);

            foreach(var stationrouteDTO in stationrouteAdminDTOs)
            {
                stationrouteDTO.StationTitle = Database.StationsRepository.Get(stationrouteDTO.StationId).Title;
            }

            return stationrouteAdminDTOs;
        }
        /// <summary>
        /// Gets an intermediate station by it's id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public StationRouteAdminDTO GetStationRoute(int id)
        {
            var stationroute = Database.StationRoutesRepository.Get(id);
            Mapper.Initialize(cfg => cfg.CreateMap<StationRoute, StationRouteAdminDTO>());
            var stationrouteAdminDTO = Mapper.Map<StationRouteAdminDTO>(stationroute);

            return stationrouteAdminDTO;
        }
        /// <summary>
        /// Updates an intermediate station.
        /// </summary>
        /// <param name="stationrouteAdminDTO"></param>
        public void UpdateStationRoute(StationRouteAdminDTO stationrouteAdminDTO)
        {
            if (Database.StationRoutesRepository.Get(stationrouteAdminDTO.Id) == null)
            {
                throw new ValidationException("Invalid identificator.", "");
            }
            if (stationrouteAdminDTO.GoingTime < 0)
            {
                throw new ValidationException("Time can not be negative.", "");
            }
            if (stationrouteAdminDTO.OrderInRoute < 0)
            {
                throw new ValidationException("Order in route can not be negative.", "");
            }
            if (Database.RoutesRepository.Get(stationrouteAdminDTO.RouteId) == null)
            {
                throw new ValidationException("Can not find specified route.", "");
            }
            if (Database.StationRoutesRepository.Find(t => t.RouteId == stationrouteAdminDTO.RouteId).Any(t => t.OrderInRoute == stationrouteAdminDTO.OrderInRoute))
            {
                throw new ValidationException("Invalid order in route.", "");
            }
            if (stationrouteAdminDTO.StandingTime < 0)
            {
                throw new ValidationException("Time can not be negative.", "");
            }
            if (Database.StationsRepository.Get(stationrouteAdminDTO.StationId) == null)
            {
                throw new ValidationException("Can not find specified station.", "");
            }
            if (stationrouteAdminDTO.TraversingCost < 0)
            {
                throw new ValidationException("Cost can not be negative.", "");
            }
            var stationroute = Database.StationRoutesRepository.Get(stationrouteAdminDTO.Id);
            Mapper.Initialize(cfg => cfg.CreateMap<StationRouteAdminDTO, StationRoute>().ForMember(t => t.Id, y => y.Ignore()));
            stationroute = Mapper.Map(stationrouteAdminDTO, stationroute);
            Database.StationRoutesRepository.Update(stationroute);
            Database.Save();
        }
        /// <summary>
        /// Creates new intermediate station.
        /// </summary>
        /// <param name="stationrouteAdminDTO"></param>
        public void CreateStationRoute(StationRouteAdminDTO stationrouteAdminDTO)
        {
            if (stationrouteAdminDTO.GoingTime < 0)
            {
                throw new ValidationException("Time can not be negative.", "");
            }
            if (stationrouteAdminDTO.OrderInRoute < 0)
            {
                throw new ValidationException("Order in route can not be negative.", "");
            }
            if (Database.RoutesRepository.Get(stationrouteAdminDTO.RouteId) == null)
            {
                throw new ValidationException("Can not find specified route.", "");
            }
            if (Database.StationRoutesRepository.Find(t => t.RouteId == stationrouteAdminDTO.RouteId).Any(t => t.OrderInRoute == stationrouteAdminDTO.OrderInRoute))
            {
                throw new ValidationException("Invalid order in route.", "");
            }
            if (stationrouteAdminDTO.StandingTime < 0)
            {
                throw new ValidationException("Time can not be negative.", "");
            }
            if (Database.StationsRepository.Get(stationrouteAdminDTO.StationId) == null)
            {
                throw new ValidationException("Can not find specified station.", "");
            }
            if (stationrouteAdminDTO.TraversingCost < 0)
            {
                throw new ValidationException("Cost can not be negative.", "");
            }
            Mapper.Initialize(cfg => cfg.CreateMap<StationRouteAdminDTO, StationRoute>().ForMember(t => t.Id, y => y.Ignore()));
            var stationroute = Mapper.Map<StationRoute>(stationrouteAdminDTO);
            Database.StationRoutesRepository.Create(stationroute);
            Database.Save();
        }
        /// <summary>
        /// Deletes intermediate station from the database.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteStationRoute(int id)
        {
            Database.StationRoutesRepository.Delete(id);
            Database.Save();
        }
        /// <summary>
        /// Gets all trains.
        /// </summary>
        /// <returns></returns>
        public List<TrainAdminDTO> GetTrains()
        {
            var trains = Database.TrainsRepository.GetAll().ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<Train, TrainAdminDTO>());
            var trainAdminDTOs = Mapper.Map<List<TrainAdminDTO>>(trains);

            return trainAdminDTOs;
        }
        /// <summary>
        /// Gets a train by it's id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TrainAdminDTO GetTrain(int id)
        {
            var train = Database.TrainsRepository.Get(id);
            Mapper.Initialize(cfg => cfg.CreateMap<Train, TrainAdminDTO>());
            var trainAdminDTO = Mapper.Map<TrainAdminDTO>(train);

            return trainAdminDTO;
        }
        /// <summary>
        /// Updates a train.
        /// </summary>
        /// <param name="trainAdminDTO"></param>
        public void UpdateTrain(TrainAdminDTO trainAdminDTO)
        {
            if (Database.TrainsRepository.Get(trainAdminDTO.Id) == null)
            {
                throw new ValidationException("Invalid identificator.", "");
            }
            if(Database.TrainsRepository.Find(t => t.Title == trainAdminDTO.Title).Any())
            {
                throw new ValidationException("The train with this title already exists.", "");
            }
            var train = Database.TrainsRepository.Get(trainAdminDTO.Id);
            Mapper.Initialize(cfg => cfg.CreateMap<TrainAdminDTO, Train>().ForMember(t => t.Id, y => y.Ignore()));
            train = Mapper.Map(trainAdminDTO, train);
            Database.TrainsRepository.Update(train);
            Database.Save();
        }
        /// <summary>
        /// Creates a new train.
        /// </summary>
        /// <param name="trainAdminDTO"></param>
        public void CreateTrain(TrainAdminDTO trainAdminDTO)
        {
            if (Database.TrainsRepository.Find(t => t.Title == trainAdminDTO.Title).Any())
            {
                throw new ValidationException("The train with this title already exists.", "");
            }
            Mapper.Initialize(cfg => cfg.CreateMap<TrainAdminDTO, Train>().ForMember(t => t.Id, y => y.Ignore()));
            var train = Mapper.Map<Train>(trainAdminDTO);
            Database.TrainsRepository.Create(train);
            Database.Save();
        }
        /// <summary>
        /// Deletes a train from the database.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteTrain(int id)
        {
            Database.TrainsRepository.Delete(id);
            Database.Save();
        }
        /// <summary>
        /// Gets all carriages for the specified train id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<CarriageAdminDTO> GetCarriagesForTrain(int id)
        {
            var carriages = Database.CarriagesRepository.Find(t => t.TrainId == id).ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<Carriage, CarriageAdminDTO>());
            var carriageAdminDTOs = Mapper.Map<List<CarriageAdminDTO>>(carriages);

            return carriageAdminDTOs;
        }
        /// <summary>
        /// Gets a carriage by it's id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CarriageAdminDTO GetCarriage(int id)
        {
            var carriage = Database.CarriagesRepository.Get(id);
            Mapper.Initialize(cfg => cfg.CreateMap<Carriage, CarriageAdminDTO>());
            var carriageAdminDTO = Mapper.Map<CarriageAdminDTO>(carriage);

            return carriageAdminDTO;
        }
        /// <summary>
        /// Updates a carriage.
        /// </summary>
        /// <param name="carriageAdminDTO"></param>
        public void UpdateCarriage(CarriageAdminDTO carriageAdminDTO)
        {
            if (Database.CarriagesRepository.Get(carriageAdminDTO.Id) == null)
            {
                throw new ValidationException("Invalid identificator.", "");
            }
            if (Database.TrainsRepository.Get(carriageAdminDTO.TrainId) == null)
            {
                throw new ValidationException("Can not find the train you specified.", "");
            }
            if (Database.CarriagesRepository.Find(t => t.TrainId == carriageAdminDTO.TrainId).Any(t => t.Number == carriageAdminDTO.Number))
            {
                throw new ValidationException("A carriage with this number is already attached to the train.", "");
            }

            var carriage = Database.CarriagesRepository.Get(carriageAdminDTO.Id);
            Mapper.Initialize(cfg => cfg.CreateMap<CarriageAdminDTO, Carriage>().ForMember(t => t.Id, y => y.Ignore()));
            carriage = Mapper.Map(carriageAdminDTO, carriage);
            Database.CarriagesRepository.Update(carriage);
            Database.Save();
        }
        /// <summary>
        /// Creates new carriage.
        /// </summary>
        /// <param name="carriageAdminDTO"></param>
        public void CreateCarriage(CarriageAdminDTO carriageAdminDTO)
        {
            if (Database.TrainsRepository.Get(carriageAdminDTO.TrainId) == null)
            {
                throw new ValidationException("Can not find the train you specified.", "");
            }
            if (Database.CarriagesRepository.Find(t => t.TrainId == carriageAdminDTO.TrainId).Any(t => t.Number == carriageAdminDTO.Number))
            {
                throw new ValidationException("A carriage with this number is already attached to the train.", "");
            }
            Mapper.Initialize(cfg => cfg.CreateMap<CarriageAdminDTO, Carriage>().ForMember(t => t.Id, y => y.Ignore()));
            var carriage = Mapper.Map<Carriage>(carriageAdminDTO);
            Database.CarriagesRepository.Create(carriage);
            Database.Save();
        }
        /// <summary>
        /// Deletes a carriage from the database.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteCarriage(int id)
        {
            Database.CarriagesRepository.Delete(id);
            Database.Save();
        }
        /// <summary>
        /// Gets all seats for the specified carriage id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<SeatAdminDTO> GetSeatsForCarriage(int id)
        {
            var seats = Database.SeatsRepository.Find(t => t.CarriageId == id).ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<Seat, SeatAdminDTO>());
            var seatAdminDTOs = Mapper.Map<List<SeatAdminDTO>>(seats);

            return seatAdminDTOs;
        }
        /// <summary>
        /// Gets a seat by it's id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SeatAdminDTO GetSeat(int id)
        {
            var seat = Database.SeatsRepository.Get(id);
            Mapper.Initialize(cfg => cfg.CreateMap<Seat, SeatAdminDTO>());
            var seatAdminDTO = Mapper.Map<SeatAdminDTO>(seat);

            return seatAdminDTO;
        }
        /// <summary>
        /// Updates a seat.
        /// </summary>
        /// <param name="seatAdminDTO"></param>
        public void UpdateSeat(SeatAdminDTO seatAdminDTO)
        {
            if (Database.SeatsRepository.Get(seatAdminDTO.Id) == null)
            {
                throw new ValidationException("Invalid identificator.", "");
            }
            if (Database.CarriagesRepository.Get(seatAdminDTO.CarriageId) == null)
            {
                throw new ValidationException("Can not find a carriage for this id.", "");
            }
            if (Database.SeatsRepository.Find(t => t.CarriageId == seatAdminDTO.CarriageId).Any(t => t.Number == seatAdminDTO.Number))
            {
                throw new ValidationException("The seat with this number already exists.", "");
            }
            var seat = Database.SeatsRepository.Get(seatAdminDTO.Id);
            Mapper.Initialize(cfg => cfg.CreateMap<SeatAdminDTO, Seat>().ForMember(t => t.Id, y => y.Ignore()));
            seat = Mapper.Map(seatAdminDTO, seat);
            Database.SeatsRepository.Update(seat);
            Database.Save();
        }
        /// <summary>
        /// Creates a seat.
        /// </summary>
        /// <param name="seatAdminDTO"></param>
        public void CreateSeat(SeatAdminDTO seatAdminDTO)
        {
            if (Database.CarriagesRepository.Get(seatAdminDTO.CarriageId) == null)
            {
                throw new ValidationException("Can not find a carriage for this id.", "");
            }
            if (Database.SeatsRepository.Find(t => t.CarriageId == seatAdminDTO.CarriageId).Any(t => t.Number == seatAdminDTO.Number))
            {
                throw new ValidationException("The seat with this number already exists.", "");
            }
            Mapper.Initialize(cfg => cfg.CreateMap<SeatAdminDTO, Seat>().ForMember(t => t.Id, y => y.Ignore()));
            var seat = Mapper.Map<Seat>(seatAdminDTO);
            Database.SeatsRepository.Create(seat);
            Database.Save();
        }
        /// <summary>
        /// Deletes a seat from the database.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteSeat(int id)
        {
            Database.SeatsRepository.Delete(id);
            Database.Save();
        }
        /// <summary>
        /// Gets all trips.
        /// </summary>
        /// <returns></returns>
        public List<TripAdminDTO> GetTrips()
        {
            var trips = Database.TripsRepository.GetAll().ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<Trip, TripAdminDTO>());
            var tripAdminDTOs = Mapper.Map<List<TripAdminDTO>>(trips);

            return tripAdminDTOs;
        }
        /// <summary>
        /// Gets a trip by it's id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TripAdminDTO GetTrip(int id)
        {
            var trip = Database.TripsRepository.Get(id);
            Mapper.Initialize(cfg => cfg.CreateMap<Trip, TripAdminDTO>());
            var tripAdminDTO = Mapper.Map<TripAdminDTO>(trip);

            return tripAdminDTO;
        }
        /// <summary>
        /// Updates a trip.
        /// </summary>
        /// <param name="tripAdminDTO"></param>
        public void UpdateTrip(TripAdminDTO tripAdminDTO)
        {
            if (Database.TripsRepository.Get(tripAdminDTO.Id) == null)
            {
                throw new ValidationException("Invalid identificator.","");
            }
            if (Database.RoutesRepository.Get(tripAdminDTO.RouteId) == null)
            {
                throw new ValidationException("Route not found.", "");
            }
            if (Database.TrainsRepository.Get(tripAdminDTO.TrainId) == null)
            {
                throw new ValidationException("Train not found.", "");
            }

            var trip = Database.TripsRepository.Get(tripAdminDTO.Id);
            Mapper.Initialize(cfg => cfg.CreateMap<TripAdminDTO, Trip>().ForMember(t => t.Id, y => y.Ignore()));
            trip = Mapper.Map(tripAdminDTO, trip);
            Database.TripsRepository.Update(trip);
            Database.Save();
        }
        /// <summary>
        /// Creates new trip.
        /// </summary>
        /// <param name="tripAdminDTO"></param>
        public void CreateTrip(TripAdminDTO tripAdminDTO)
        {
            if (Database.RoutesRepository.Get(tripAdminDTO.RouteId) == null)
            {
                throw new ValidationException("Route not found.", "");
            }
            if (Database.TrainsRepository.Get(tripAdminDTO.TrainId) == null)
            {
                throw new ValidationException("Train not found.", "");
            }
            Mapper.Initialize(cfg => cfg.CreateMap<TripAdminDTO, Trip>().ForMember(t => t.Id, y => y.Ignore()));
            var trip = Mapper.Map<Trip>(tripAdminDTO);
            Database.TripsRepository.Create(trip);
            Database.Save();
        }
        /// <summary>
        /// Deletes a trip from the database.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteTrip(int id)
        {
            Database.TripsRepository.Delete(id);
            Database.Save();
        }
        /// <summary>
        /// Gets all tickets.
        /// </summary>
        /// <returns></returns>
        public List<TicketAdminDTO> GetTickets()
        {
            var tickets = Database.TicketsRepository.GetAll().ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<Ticket, TicketAdminDTO>());
            var ticketAdminDTOs = Mapper.Map<List<TicketAdminDTO>>(tickets);

            return ticketAdminDTOs;
        }
        /// <summary>
        /// Gets a ticket by it's id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TicketAdminDTO GetTicket(int id)
        {
            var ticket = Database.TicketsRepository.Get(id);
            Mapper.Initialize(cfg => cfg.CreateMap<Ticket, TicketAdminDTO>());
            var ticketAdminDTO = Mapper.Map<TicketAdminDTO>(ticket);

            return ticketAdminDTO;
        }
        /// <summary>
        /// Updates a ticket.
        /// </summary>
        /// <param name="ticketAdminDTO"></param>
        public void UpdateTicket(TicketAdminDTO ticketAdminDTO)
        {
            if (Database.TicketsRepository.Get(ticketAdminDTO.Id) == null)
            {
                throw new ValidationException("Can not find a ticket.", "");
            }
            if (Database.UserManager.FindById(ticketAdminDTO.ApplicationUserId) == null)
            {
                throw new ValidationException("Can not find a user.", "");
            }
            if (Database.TripsRepository.Get(ticketAdminDTO.TripId) == null)
            {
                throw new ValidationException("Can not find a trip.", "");
            }
            if (Database.CarriagesRepository.Get(ticketAdminDTO.CarriageId) == null)
            {
                throw new ValidationException("Can not find a carriage.", "");
            }
            if (Database.SeatsRepository.Get(ticketAdminDTO.SeatId) == null)
            {
                throw new ValidationException("Can not find a seat.", "");
            }
            if (ticketAdminDTO.Cost < 0)
            {
                throw new ValidationException("Cost can not be negative.", "");
            }
            var ticket = Database.TicketsRepository.Get(ticketAdminDTO.Id);
            Mapper.Initialize(cfg => cfg.CreateMap<TicketAdminDTO, Ticket>().ForMember(t => t.Id, y => y.Ignore()));
            ticket = Mapper.Map(ticketAdminDTO, ticket);
            Database.TicketsRepository.Update(ticket);
            Database.Save();
        }
        /// <summary>
        /// Creates new ticket.
        /// </summary>
        /// <param name="ticketAdminDTO"></param>
        public void CreateTicket(TicketAdminDTO ticketAdminDTO)
        {
            if (Database.UserManager.FindById(ticketAdminDTO.ApplicationUserId) == null)
            {
                throw new ValidationException("Can not find a user.", "");
            }
            if (Database.TripsRepository.Get(ticketAdminDTO.TripId) == null)
            {
                throw new ValidationException("Can not find a trip.", "");
            }
            if (Database.CarriagesRepository.Get(ticketAdminDTO.CarriageId) == null)
            {
                throw new ValidationException("Can not find a carriage.", "");
            }
            if (Database.SeatsRepository.Get(ticketAdminDTO.SeatId) == null)
            {
                throw new ValidationException("Can not find a seat.", "");
            }
            if (ticketAdminDTO.Cost < 0)
            {
                throw new ValidationException("Cost can not be negative.", "");
            }
            Mapper.Initialize(cfg => cfg.CreateMap<TicketAdminDTO, Ticket>().ForMember(t => t.Id, y => y.Ignore()));
            var ticket = Mapper.Map<Ticket>(ticketAdminDTO);
            Database.TicketsRepository.Create(ticket);
            Database.Save();
        }
        /// <summary>
        /// Deletes a ticket from the database.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteTicket(int id)
        {
            Database.TicketsRepository.Delete(id);
            Database.Save();
        }

    }
}
