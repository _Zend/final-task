﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Ninject;
using WebApp.BLL.DTO;
using WebApp.BLL.Infrastructure;
using WebApp.BLL.Util;
using WebApp.DAL.EF;
using WebApp.DAL.Interfaces;
using WebApp.DAL.Models.Domain;

namespace WebApp.BLL.Services
{
    /// <summary>
    /// Ticket service is working with user orders.
    /// </summary>
    public class TicketService
    {
        protected IUnitOfWork Database { get; }

        public TicketService(IUnitOfWork uow)
        {
            Database = uow;
        }
        public TicketService()
        {
            var kernel = new NinjectDependencyResolver(new StandardKernel());
            Database = kernel.GetService<IUnitOfWork>();
        }
        /// <summary>
        /// Gets all seats for the specified carriage id.
        /// </summary>
        /// <param name="carriageId"></param>
        /// <returns></returns>
        public List<SeatDTO> GetSeats(int carriageId)
        {
            var allSeats = Database.SeatsRepository.GetAll()
                .Where(t => t.CarriageId == carriageId).ToList();

            var reservedSeats = Database.TicketsRepository.GetAll()
                .Where(t => t.Trip.StartDateTime > DateTime.Now)
                .Select(t => t.Seat)
                .ToList();

            var freeSeats = allSeats.Except(reservedSeats).ToList();

            Mapper.Initialize(cfg => cfg.CreateMap<Seat, SeatDTO>());
            var freeSeatsDTO = Mapper.Map<List<Seat>, List<SeatDTO>>(freeSeats);

            return freeSeatsDTO;
        }
        /// <summary>
        /// Gets all carriages for the specified train id.
        /// </summary>
        /// <param name="trainId"></param>
        /// <returns></returns>
        public List<CarriageDTO> GetCarriages(int trainId)
        {
            var carriages = Database.CarriagesRepository.GetAll()
                .Where(t => t.TrainId == trainId)
                .ToList();

            Mapper.Initialize(cfg => cfg.CreateMap<Carriage, CarriageDTO>());
            var carriageDTOs = Mapper.Map<List<Carriage>, List<CarriageDTO>>(carriages);

            return carriageDTOs;
        }
        /// <summary>
        /// Performs a ticket registration and returns it's id if successed.
        /// </summary>
        /// <param name="orderDTO"></param>
        /// <returns></returns>
        public int Buy(OrderDTO orderDTO)
        {
            var user = Database.UserManager.FindByName(orderDTO.UserName);
            var trip = Database.TripsRepository.Get(orderDTO.TripId);

            if (trip == null)
            {
                throw new ValidationException("Could not find a trip." , "");
            }
            if (orderDTO.Cost < 0)
            {
                throw new ValidationException("Invalid cost.", "");
            }
            if (!Database.CarriagesRepository.GetAll()
                .Where(t => t.TrainId == trip.TrainId)
                .Contains(Database.CarriagesRepository.Get(orderDTO.CarriageId)))
            {
                throw new ValidationException("Invalid carriage.", "");
            }
            if (Database.SeatsRepository.Get(orderDTO.SeatId) == null || Database.TicketsRepository.Find(t => t.SeatId == orderDTO.SeatId && t.Trip.StartDateTime > DateTime.Now).Any())
            {
                throw new ValidationException("Seat is already bought or does not exist.", "");
            }

            var cost = orderDTO.Cost - orderDTO.userdiscount;

            var newTicket = new Ticket()
            {
                ApplicationUser = user,
                ApplicationUserId = user.Id,
                CarriageId = orderDTO.CarriageId,
                Carriage = Database.CarriagesRepository.Get(orderDTO.CarriageId),
                SeatId = orderDTO.SeatId,
                Seat = Database.SeatsRepository.Get(orderDTO.SeatId),
                TripId = orderDTO.TripId,
                Trip = Database.TripsRepository.Get(orderDTO.TripId),
                Cost = cost
            };

            user.Bank -= (int) cost;
            if (user.Bank < 0)
            {
                user.Bank += (int) cost;
                throw new BankException("Not enough money to buy a ticket.");
            }
            
            Database.TicketsRepository.Create(newTicket);
            Database.Save();

            var trainId = Database.TripsRepository.Get(orderDTO.TripId).TrainId;
            var train = Database.TrainsRepository.Get(trainId);

            var ticketInfo = new TicketInfo()
            {
                ArrivingStation = orderDTO.stationarriving,
                ArrivingDate = orderDTO.arrivingdate,
                BoardingDate = orderDTO.boardingdate,
                BoardingStation = orderDTO.stationboarding,
                CarriageNumber = Database.CarriagesRepository.Get(orderDTO.CarriageId).Number,
                Cost = cost,
                SeatNumber = Database.SeatsRepository.Get(orderDTO.SeatId).Number,
                TimeInRoute = orderDTO.timeinroute,
                TrainId = train.Id,
                TrainTitle = train.Title,
                UserName = user.UserName

            };

            Database.TicketInfosRepository.Create(ticketInfo);
            
            Database.Save();

            return ticketInfo.Id;
        }
        /// <summary>
        /// Gets a ticket by it's id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TicketInfoDTO GetTicket(int id)
        {
            var ticketInfo = Database.TicketInfosRepository.Get(id);

            Mapper.Initialize(cfg => cfg.CreateMap<TicketInfo, TicketInfoDTO>());
            var ticketInfoDTO = Mapper.Map<TicketInfoDTO>(ticketInfo);

            return ticketInfoDTO;
        }
        /// <summary>
        /// Gets a list of all tickets that the user had ordered.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public List<TicketInfoDTO> GetTickets(string userName)
        {
            var user = Database.UserManager.FindByName(userName);
            var ticketInfos = Database.TicketInfosRepository.Find(t => t.UserName == user.UserName);

            Mapper.Initialize(cfg => cfg.CreateMap<TicketInfo, TicketInfoDTO>());
            var ticketInfoDTOs = Mapper.Map<List<TicketInfoDTO>>(ticketInfos);

            return ticketInfoDTOs;
        }
    }
}
