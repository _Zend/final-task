﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebApp.BLL.DTO;
using WebApp.BLL.Infrastructure;
using WebApp.DAL.EF;
using WebApp.DAL.Models.Domain;
using AutoMapper;
using Ninject;
using WebApp.BLL.Util;
using WebApp.DAL.Interfaces;

namespace WebApp.BLL.Services
{
    /// <summary>
    /// TrainService is working with database and user requested stations in order
    /// to show all trips the user wants.
    /// </summary>
    public class TrainService
    {
        protected IUnitOfWork Database { get; }

        public TrainService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public TrainService()
        {
            var kernel = new NinjectDependencyResolver(new StandardKernel());
            Database = kernel.GetService<IUnitOfWork>();
        }
        /// <summary>
        /// The method returns a list of trips between specified by the user stations at the specified date.
        /// </summary>
        /// <param name="stations"></param>
        /// <returns></returns>
        public List<TripDTO> Find(StationsDTO stations)
        {
            // Getting a list of routes between stations.
            var stationFrom = new Station();
            var stationTo = new Station();
            // Find all trains, which are in trips on routes.
            try
            {
                stationFrom = Database.StationsRepository.GetAll()
                    .First(t => string.Equals(t.Title, stations.StationFrom.Title, StringComparison.CurrentCultureIgnoreCase));

                stationTo = Database.StationsRepository.GetAll()
                    .First(t => string.Equals(t.Title, stations.StationTo.Title, StringComparison.CurrentCultureIgnoreCase));
            }
            catch (InvalidOperationException)
            {
                throw new ValidationException(@"Could not find stations you specified.", "");
            }

            var routesBetweenStations = new List<Route>();

            // Find routes if stations are the first and the last on the route.

            if (stationFrom.RoutesFrom.Where(t => t.IsDeleted == false).Intersect(stationTo.RoutesTo.Where(t => t.IsDeleted == false)).Any())
            {
                routesBetweenStations.AddRange(stationFrom.RoutesFrom.Intersect(stationTo.RoutesTo).ToList());
            }
            // Find routes if the first station is first but the second station is somewhere inside the route.
            if (stationFrom.RoutesFrom.Any(t => t.IsDeleted == false))
            {
                routesBetweenStations.AddRange(Database.StationRoutesRepository.GetAll()
                    .Where(t => t.Station == stationTo)
                    .Select(t => t.Route)
                    .Where(t => t.StationFrom == stationFrom)
                    .ToList());
            }
            // Find routes if the second station is last but the first station is somewhere inside the route.
            if (stationTo.RoutesTo.Any(t => t.IsDeleted == false))
            {
                routesBetweenStations.AddRange(Database.StationRoutesRepository.GetAll()
                    .Where(t => t.Station == stationFrom)
                    .Select(t => t.Route)
                    .Where(t => t.StationTo == stationTo)
                    .ToList());
            }

            // Find routes if both stations are inside the route.
            
            var groups = Database.StationRoutesRepository.GetAll()
                .OrderBy(t => t.OrderInRoute)
                .GroupBy(t => t.Route)
                .ToList();

            foreach (var group in groups)
            {
                var stationRoutesList = new List<StationRoute>();

                foreach (var stationRoute in group)
                {
                    stationRoutesList.Add(stationRoute);
                }

                var foundStationFrom = stationRoutesList.Find(t => t.Station == stationFrom);
                var foundStationTo = stationRoutesList.Find(t => t.Station == stationTo);

                if (foundStationTo != null && foundStationFrom != null &&
                    foundStationFrom.OrderInRoute < foundStationTo.OrderInRoute)
                {
                    routesBetweenStations.Add(group.Key);
                }
            }
            
            routesBetweenStations = routesBetweenStations.Distinct(new RoutesEqualityComparer()).ToList();

            // Forming a TripsDTO LIST
            if (routesBetweenStations.Any())
            {
                var tripsDTO = new List<TripDTO>();

                var trips = Database.TripsRepository.GetAll()
                    .Where(t => routesBetweenStations.Contains(t.Route))
                    .Where(t => t.StartDateTime.ToShortDateString() == stations.Date.ToShortDateString())
                    .ToList();

                foreach (var trip in trips)
                {
                    var stationRoutesForTimeAndCost = Database.StationRoutesRepository.GetAll()  
                        .Where(t => t.Route == trip.Route).ToList();

                    var timeInRoute = stationRoutesForTimeAndCost.Sum(t => t.GoingTime) + stationRoutesForTimeAndCost.Sum(t => t.StandingTime) + trip.Route.TimeInRoute; 
                    var totalCost = 0;

                    var dateTimeOfArriving = trip.StartDateTime.AddMinutes(timeInRoute); 

                    var freeSeats = new Dictionary<int, int>();
                    var carriagesForTrainInTrip = Database.CarriagesRepository.GetAll().Where(t => t.TrainId == trip.TrainId);

                    foreach (var carriage in carriagesForTrainInTrip)
                    {
                        var allSlots = Database.SeatsRepository.GetAll().Where(t => t.Carriage == carriage);
                        var reservedSlots = Database.TicketsRepository.GetAll()
                            .Where(t => t.Trip == trip)
                            .Where(t => t.Carriage == carriage)
                            .Select(t => t.Seat)
                            .ToList();
                        var freeSlots = allSlots.Except(reservedSlots).Count();

                        if (freeSeats.ContainsKey((int) carriage.Type))
                        {
                            freeSeats[(int) carriage.Type] += freeSlots;
                        }
                        else
                        {
                            freeSeats.Add((int) carriage.Type, freeSlots);
                        }
                    }

                    var traverseStations = new Dictionary<int, StationInfoDTO>();

                    var boardingStation = new StationInfoDTO();
                    var arrivingStation = new StationInfoDTO();

                    var boardingDateTime = new DateTime();
                    var arrivingDateTime = new DateTime();

                    var timeInUserRoute = new TimeSpan(0, 0, 0);

                    foreach (var stationRouteItem in stationRoutesForTimeAndCost)
                    {
                        if (stationRouteItem.Station == stationFrom)
                        {
                            break;
                        }
                        boardingDateTime = boardingDateTime.AddMinutes(stationRouteItem.GoingTime + stationRouteItem.StandingTime);
                    }

                    foreach (var stationRouteItem in stationRoutesForTimeAndCost)
                    {
                        if (stationRouteItem.Station == stationTo)
                        {
                            break;
                        }
                        arrivingDateTime = boardingDateTime.AddMinutes(stationRouteItem.GoingTime + stationRouteItem.StandingTime);
                    }

                    if (stationFrom.RoutesFrom.Intersect(stationTo.RoutesTo).Any())
                    {
                        boardingStation.Title = stationFrom.Title;
                        boardingStation.DateTimeOfStart = trip.StartDateTime;
                        boardingStation.DateTimeOfLeaving = trip.StartDateTime;

                        arrivingStation.Title = stationTo.Title;
                        arrivingStation.DateTimeOfStart = dateTimeOfArriving;
                        arrivingStation.DateTimeOfLeaving = dateTimeOfArriving;

                        totalCost = stationRoutesForTimeAndCost.Sum(t => t.TraversingCost) + trip.Route.Cost;

                        timeInUserRoute = TimeSpan.FromMinutes(trip.Route.TimeInRoute)
                            .Add(TimeSpan.FromMinutes(stationRoutesForTimeAndCost.OrderBy(t => t.OrderInRoute).Sum(t => t.GoingTime)))
                            .Add(TimeSpan.FromMinutes(stationRoutesForTimeAndCost.OrderBy(t => t.OrderInRoute).Sum(t => t.StandingTime)));
                    }
                    else if (stationFrom.RoutesFrom.Any())
                    {
                        boardingStation.Title = stationFrom.Title;
                        boardingStation.DateTimeOfStart = trip.StartDateTime;
                        boardingStation.DateTimeOfLeaving = trip.StartDateTime;

                        arrivingStation.Title = stationRoutesForTimeAndCost.First(t => t.Station.Id == stationTo.Id).Station.Title;
                        arrivingStation.DateTimeOfStart = arrivingDateTime;
                        arrivingStation.DateTimeOfLeaving = arrivingDateTime;

                        timeInUserRoute = new TimeSpan(0,0,0);

                        foreach (var stationRouteForTime in stationRoutesForTimeAndCost)
                        {
                            if (stationRouteForTime.Station.Id == stationTo.Id)
                            {
                                totalCost += stationRouteForTime.TraversingCost;
                                timeInUserRoute += TimeSpan.FromMinutes(stationRouteForTime.GoingTime);
                                break;
                            }
                            totalCost += stationRouteForTime.TraversingCost;
                            timeInUserRoute += TimeSpan.FromMinutes(stationRouteForTime.GoingTime);
                            timeInUserRoute += TimeSpan.FromMinutes(stationRouteForTime.StandingTime);
                        }
                        
                    }
                    else if (stationTo.RoutesTo.Any())
                    {
                        arrivingStation.Title = stationTo.Title;
                        arrivingStation.DateTimeOfStart = dateTimeOfArriving;
                        arrivingStation.DateTimeOfLeaving = dateTimeOfArriving;

                        boardingStation.Title = stationRoutesForTimeAndCost.First(t => t.Station.Id == stationFrom.Id).Station.Title;
                        boardingStation.DateTimeOfStart = boardingDateTime;
                        boardingStation.DateTimeOfLeaving = boardingDateTime;

                        timeInUserRoute = TimeSpan.FromMinutes(trip.Route.TimeInRoute);

                        stationRoutesForTimeAndCost.Reverse();

                        foreach (var stationRouteForTime in stationRoutesForTimeAndCost)
                        {
                            if (stationRouteForTime.Station.Id == stationFrom.Id)
                            {
                                totalCost += stationRouteForTime.TraversingCost;
                                break;
                            }
                            totalCost += stationRouteForTime.TraversingCost;
                            timeInUserRoute += TimeSpan.FromMinutes(stationRouteForTime.GoingTime);
                            timeInUserRoute += TimeSpan.FromMinutes(stationRouteForTime.StandingTime);
                        }

                        stationRoutesForTimeAndCost.Reverse();
                    }
                    else
                    {
                        boardingStation.Title = stationRoutesForTimeAndCost.First(t => t.Station.Id == stationFrom.Id).Station.Title;
                        boardingStation.DateTimeOfStart = boardingDateTime;
                        boardingStation.DateTimeOfLeaving = boardingDateTime;

                        arrivingStation.Title = stationRoutesForTimeAndCost.First(t => t.Station.Id == stationTo.Id).Station.Title;
                        arrivingStation.DateTimeOfStart = arrivingDateTime;
                        arrivingStation.DateTimeOfLeaving = arrivingDateTime;

                        timeInUserRoute = new TimeSpan(0, 0, 0);

                        var stationFromIndex = stationRoutesForTimeAndCost.OrderBy(t => t.OrderInRoute).ToList()
                            .FindIndex(t => t.Station.Id == stationFrom.Id);
                        var stationFromList = stationRoutesForTimeAndCost.Skip(stationFromIndex + 1).ToList();

                        foreach (var stationFromItem in stationFromList)
                        {
                            if (stationFromItem.Station.Id == stationTo.Id)
                            {
                                timeInUserRoute += TimeSpan.FromMinutes(stationFromItem.GoingTime);
                                break;
                            }
                            totalCost += stationFromItem.TraversingCost;
                            timeInUserRoute += TimeSpan.FromMinutes(stationFromItem.GoingTime);
                            timeInUserRoute += TimeSpan.FromMinutes(stationFromItem.StandingTime);
                        }
                    }

                    traverseStations.Add(0, new StationInfoDTO()
                    {
                        DateTimeOfStart = trip.StartDateTime,
                        DateTimeOfLeaving = trip.StartDateTime,
                        TimeOfWaiting = new TimeSpan(0, 0, 0),
                        Title = trip.Route.StationFrom.Title
                    });

                    foreach (var stationRouteForTraverseRoute in stationRoutesForTimeAndCost)
                    {
                        traverseStations.Add(stationRouteForTraverseRoute.OrderInRoute, new StationInfoDTO()
                        {
                            DateTimeOfStart = traverseStations.Values.ToList().Last()
                                .DateTimeOfLeaving
                                .Add(TimeSpan.FromMinutes(stationRouteForTraverseRoute.GoingTime)),
                            DateTimeOfLeaving = traverseStations.Values.ToList().Last()
                                .DateTimeOfLeaving
                                .Add(TimeSpan.FromMinutes(stationRouteForTraverseRoute.GoingTime))
                                .Add(TimeSpan.FromMinutes(stationRouteForTraverseRoute.StandingTime)),
                            TimeOfWaiting = TimeSpan.FromMinutes(stationRouteForTraverseRoute.StandingTime),
                            Title = stationRouteForTraverseRoute.Station.Title
                        });
                    }

                    traverseStations.Add(traverseStations.Keys.Max() + 1, new StationInfoDTO()
                    {
                        DateTimeOfStart = traverseStations.Values.ToList().Last()
                            .DateTimeOfLeaving
                            .Add(TimeSpan.FromMinutes(trip.Route.TimeInRoute)),
                        DateTimeOfLeaving = traverseStations.Values.ToList().Last()
                            .DateTimeOfLeaving
                            .Add(TimeSpan.FromMinutes(trip.Route.TimeInRoute)),
                        TimeOfWaiting = new TimeSpan(0, 0, 0),
                        Title = trip.Route.StationTo.Title
                    });

                    tripsDTO.Add(new TripDTO()
                    {
                        TrainId = trip.Train.Id,
                        TrainTitle = trip.Train.Title,
                        TripId = trip.Id,
                        StationFrom = new StationInfoDTO()
                        {
                            DateTimeOfLeaving = trip.StartDateTime,
                            DateTimeOfStart = trip.StartDateTime,
                            TimeOfWaiting = new TimeSpan(0,0,0),
                            Title = trip.Route.StationFrom.Title
                        },
                        StationTo = new StationInfoDTO()
                        {
                            DateTimeOfStart = dateTimeOfArriving,
                            DateTimeOfLeaving = dateTimeOfArriving,
                            TimeOfWaiting =  new TimeSpan(0,0,0),
                            Title = trip.Route.StationTo.Title
                        },
                        Cost = totalCost,
                        DateTimeOfStart = trip.StartDateTime,
                        DateTimeOfArriving = dateTimeOfArriving,
                        FreeSeats = freeSeats,
                        StationArriving = arrivingStation,
                        StationBoarding = boardingStation,
                        TraverseStations = traverseStations,
                        TimeInRoute = timeInUserRoute,
                        FullTimeInRoute = TimeSpan.FromMinutes(timeInRoute)
                    });
                }

                return tripsDTO;
            }
            else
            {
                return new List<TripDTO>();
            }
        }
        /// <summary>
        /// The method returns a train that is dependent on the trip.
        /// </summary>
        /// <param name="tripId"></param>
        /// <returns></returns>
        public TrainDTO GetTrainByTripId(int tripId)
        {
            var trip = Database.TripsRepository.Get(tripId);

            var train = Database.TrainsRepository.GetAll()
                .First(t => t.Id == trip.TrainId);

            Mapper.Initialize(cfg => cfg.CreateMap<Train, TrainDTO>());
            var trainDTO = Mapper.Map<Train, TrainDTO>(train);

            return trainDTO;
        }
        
        public List<StationDTO> GetStations()
        {
            var stations = Database.StationsRepository.GetAll().ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<Station, StationDTO>());
            var stationDTOs = Mapper.Map<List<StationDTO>>(stations);

            return stationDTOs;
        }
    }
}
