﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Ninject;
using WebApp.BLL.Util;
using WebApp.DAL.EF;
using WebApp.DAL.Interfaces;

namespace WebApp.BLL.Services
{
    /// <summary>
    /// Discount service is working with users allowing them to get special status.
    /// In order to get benefits a user must provide his documents identificator.
    /// </summary>
    public class DiscountService
    {
        protected IUnitOfWork Database { get; }

        public DiscountService(IUnitOfWork uow)
        {
            Database = uow;
        }
        /// <summary>
        /// Adds a user to "Special" group of students or disabled people.
        /// </summary>
        /// <param name="userName"></param>
        public void AddUserToSpecialGroup(string userName)
        {
            var user = Database.UserManager.FindByName(userName);
            Database.UserManager.AddToRole(user.Id, "special");
            Database.Save();
        }

        public DiscountService()
        {
            var kernel = new NinjectDependencyResolver(new StandardKernel());
            Database = kernel.GetService<IUnitOfWork>();
        }
    }
}
