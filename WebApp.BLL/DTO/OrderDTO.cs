﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.DAL.Models.Identity;

namespace WebApp.BLL.DTO
{
    public class OrderDTO
    {
        public int TripId { get; set; }
        public int CarriageId { get; set; }
        public int SeatId { get; set; }
        public double Cost { get; set; }
        public int userdiscount { get; set; }
        public string UserName { get; set; }
        public string stationboarding { get; set; }
        public string stationarriving { get; set; }
        public DateTime arrivingdate { get; set; }
        public DateTime boardingdate { get; set; }
        public TimeSpan timeinroute { get; set; }
    }
}
