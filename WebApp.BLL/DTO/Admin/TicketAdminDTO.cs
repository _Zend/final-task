﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.BLL.DTO.Admin
{
    public class TicketAdminDTO
    {
        public int Id { get; set; }
        public int TripId { get; set; }
        public int CarriageId { get; set; }
        public int SeatId { get; set; }
        public double Cost { get; set; }
        public string ApplicationUserId { get; set; }
        public bool IsDeleted { get; set; }
    }
}
