﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.BLL.DTO.Admin
{
    public class SeatAdminDTO
    {
        public int Id { get; set; }
        public int CarriageId { get; set; }
        public int Number { get; set; }
        public bool IsDeleted { get; set; }
    }
}
