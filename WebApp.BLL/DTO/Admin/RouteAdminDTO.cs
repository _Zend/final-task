﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.BLL.DTO.Admin
{
    public class RouteAdminDTO
    {
        public int Id { get; set; }
        public int StationIdFrom { get; set; }
        public int StationIdTo { get; set; }
        public int Cost { get; set; }
        public int TimeInRoute { get; set; }
        public string StationFrom { get; set; }
        public string StationTo { get; set; }
        public bool IsDeleted { get; set; }
    }
}
