﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.DAL.Models.Domain;

namespace WebApp.BLL.DTO.Admin
{
    public class CarriageAdminDTO
    {
        public int Id { get; set; }
        public CarriageType Type { get; set; }
        public int TrainId { get; set; }
        public int Number { get; set; }
        public bool IsDeleted { get; set; }
    }
}
