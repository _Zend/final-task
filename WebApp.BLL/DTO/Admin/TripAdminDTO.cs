﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.BLL.DTO.Admin
{
    public class TripAdminDTO
    {
        public int Id { get; set; }
        public int TrainId { get; set; }
        public int RouteId { get; set; }
        public DateTime StartDateTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}
