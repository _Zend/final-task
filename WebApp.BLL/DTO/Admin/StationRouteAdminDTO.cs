﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.BLL.DTO.Admin
{
    public class StationRouteAdminDTO
    {
        public int Id { get; set; }
        public int StationId { get; set; }
        public string StationTitle { get; set; }
        public int RouteId { get; set; }
        public int GoingTime { get; set; }
        public int StandingTime { get; set; }
        public int OrderInRoute { get; set; }
        public int TraversingCost { get; set; }
        public bool IsDeleted { get; set; }
    }
}
