﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.BLL.DTO
{
    public class StationInfoDTO
    {
        public string Title { get; set; }
        public DateTime DateTimeOfStart { get; set; }
        public DateTime DateTimeOfLeaving { get; set; }
        public TimeSpan TimeOfWaiting { get; set; }
    }
}
