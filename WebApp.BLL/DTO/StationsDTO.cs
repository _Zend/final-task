﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.BLL.DTO
{
    public class StationsDTO
    {
        public StationDTO StationFrom { get; set; }
        public StationDTO StationTo { get; set; }
        public DateTime Date { get; set; }
    }
}
