﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.DAL.Models.Identity;

namespace WebApp.BLL.DTO
{
    public class TicketDTO
    {
        public int Id { get; set; }
        public int TrainId { get; set; }
        public string TrainTitle { get; set; }
        public int CarriageNumber { get; set; }
        public int SeatNumber { get; set; }
        public double Cost { get; set; }
        public StationDTO BoardingStation { get; set; }
        public StationDTO ArrivingStation { get; set; }
        public DateTime ArrivingDate { get; set; }
        public DateTime BoardingDate { get; set; }
        public TimeSpan TimeInRoute { get; set; }
        public ApplicationUser User { get; set; }

    }
}
