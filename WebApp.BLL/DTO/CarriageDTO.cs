﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.BLL.DTO
{
    public class CarriageDTO
    {
        public int Id { get; set; }
        public int Number { get; set; }
    }
}
