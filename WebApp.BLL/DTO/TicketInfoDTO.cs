﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.BLL.DTO
{
    public class TicketInfoDTO
    {
        public int Id { get; set; }
        public int TrainId { get; set; }
        public string TrainTitle { get; set; }
        public int CarriageNumber { get; set; }
        public int SeatNumber { get; set; }
        public double Cost { get; set; }
        public string BoardingStation { get; set; }
        public string ArrivingStation { get; set; }
        public DateTime ArrivingDate { get; set; }
        public DateTime BoardingDate { get; set; }
        public TimeSpan TimeInRoute { get; set; }
        public string UserName { get; set; }
    }
}
