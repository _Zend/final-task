﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.BLL.DTO
{
    public class TripDTO
    {
        public int TrainId { get; set; }
        public int TripId { get; set; }
        public string TrainTitle { get; set; }
        public StationInfoDTO StationFrom { get; set; }
        public StationInfoDTO StationTo { get; set; }
        public StationInfoDTO StationBoarding { get; set; }
        public StationInfoDTO StationArriving { get; set; }
        public DateTime DateTimeOfStart { get; set; }
        public DateTime DateTimeOfArriving { get; set; }
        public TimeSpan TimeInRoute { get; set; }
        public double Cost { get; set; }
        public Dictionary<int, int> FreeSeats { get; set; } // int - type, int - amount
        public Dictionary<int, StationInfoDTO> TraverseStations { get; set; } // int - order
        public TimeSpan FullTimeInRoute { get; set; }
    }
}
