﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.DAL.Models.Domain;

namespace WebApp.BLL.DTO
{
    class RoutesEqualityComparer : IEqualityComparer<Route>
    {
        public bool Equals(Route x, Route y)
        {
            if (x.Id == y.Id)
            {
                return true;
            }
            return false;
        }

        public int GetHashCode(Route obj)
        {
            return obj.Id * 1337 + 228 + (int) DateTime.Now.Ticks / 1000;
        }
    }
}
