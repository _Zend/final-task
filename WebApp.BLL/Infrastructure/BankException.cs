﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.BLL.Infrastructure
{
    /// <summary>
    /// Thrown when the user has not enough money on his bank account
    /// and wants to perform an action that requires money they dont have.
    /// </summary>
    public class BankException : Exception
    {
        public BankException(string message) : base(message)
        {
        }
    }
}
